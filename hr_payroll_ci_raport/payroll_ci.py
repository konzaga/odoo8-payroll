# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2014 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_payroll_ci_raport
# ##############################################################################

import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta


from openerp.osv import osv,fields
from openerp import api,modules

class hr_payroll(osv.osv):
    _name="hr.payroll"
    _description="Livre de paie"
    _columns={
            "name":fields.char("Libellé",size=128),
            "date_from":fields.date('Date de début',required=True),
            "date_to":fields.date('Date de fin',required=True),
            "line_ids":fields.one2many('hr.payroll.line','hr_payroll_id',"Lignes de livre de paie"),
            "company_id":fields.many2one('res.company','Compagnie'),
            'partner_id':fields.many2one('res.partner','Parténaire'),
        }
    
    _defaults = {  
        'date_from': lambda *a: time.strftime('%Y-%m-01'),  
        'date_to': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
        "company_i'd":1,
        'partner_id':1,
        }
    
    def print_payroll(self, cr, uid, ids, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        
        self.compute_hr_payroll(cr, uid, ids, context)
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'hr_payroll_ci_raport.report_payroll', context=context)
    
    
    def get_amount_by_code(self,cr,uid,line_ids,code,context=None):
        for line in line_ids:
            if line.code==code :
                return line.total
            
    def get_amount_by_category(self,cr,uid,line_ids,category,context=None):
        rcategory_obj=self.pool.get('hr.salary.rule.category')
        category_id=rcategory_obj.search(cr,uid,[('code',"=",category)])
        montant=0        
        if category_id :
            for line in line_ids :
                if line.category_id == category_id[0] :
                    montant+=line.total
        return montant
    
    def compute_hr_payroll(self,cr,uid,ids,context=None):

        hpslip_obj=self.pool.get('hr.payslip')
        hpline_obj=self.pool.get('hr.payroll.line')
        for i in self.browse(cr, uid, ids, context):
            if i.line_ids :
                cr.execute("DELETE FROM hr_payroll_line WHERE hr_payroll_id=%s"%i.id)
                cr.commit()
            res={'value':{'line_ids':False}}
            slip_ids=hpslip_obj.search(cr,uid,[('date_from','<=',i.date_from),('date_to','>=',i.date_to)])
            lines={}
            
            for slip in hpslip_obj.browse(cr,uid,slip_ids,context):

                vals={
                        'name':slip.employee_id.name,
                        'salaire_base':self.get_amount_by_code(cr, uid, slip.line_ids, "BASE", context),
                        "sursalaire":self.get_amount_by_code(cr, uid, slip.line_ids, "SURSA", context),
                        "prime_imposable":self.get_amount_by_category(cr, uid, slip.line_ids, "PRIM", context),
                        "hs":self.get_amount_by_category(cr, uid, slip.line_ids, "HSUPP", context),
                        "avantage_nature":self.get_amount_by_category(cr, uid, slip.line_ids, "AVTG", context),
                        "salbrut_imposable":self.get_amount_by_code(cr, uid, slip.line_ids, "BRUT", context),
                        "its":self.get_amount_by_code(cr, uid, slip.line_ids, "ITS", context),
                        "igr":self.get_amount_by_code(cr, uid, slip.line_ids, "IGR", context),
                        "cn":self.get_amount_by_code(cr, uid, slip.line_ids, "CN", context),
                        "cnps":self.get_amount_by_code(cr, uid, slip.line_ids, "CNPS", context),
                        "nuit":self.get_amount_by_code(cr, uid, slip.line_ids, "NUIT", context),
                        "total_retenues":self.get_amount_by_code(cr, uid, slip.line_ids, "RET", context),
                        "avance":self.get_amount_by_code(cr, uid, slip.line_ids, "EMP", context),
                        "indem_non_impo":self.get_amount_by_code(cr, uid, slip.line_ids, "INDMNI", context),
                        "net_a_paye":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),
                        "hr_payroll_id":i.id,
                        }
                line_id=hpline_obj.create(cr,uid,vals,context)
                
hr_payroll()


class hr_payroll_line(osv.osv):
    _name="hr.payroll.line"
    _description="Ligne de livre de paie"
    _columns = {
                "name":fields.char("Nom & Prénoms",size=128,required=True),
                "salaire_base":fields.integer("Salaire de base",requred=True),
                "sursalaire":fields.integer("Sursalaire",required=True),
                "prime_imposable":fields.integer("Primes Imposables",required=True),
                "hs":fields.integer("Heures Supplémentaires",required=True),
                "avantage_nature":fields.integer("Avantages en nature",required=True),
                "salbrut_imposable":fields.integer("Salaire Brut imposable",requried=True),
                "its":fields.integer("ITS",required=True),
                "igr":fields.integer("IGR",required=True),
                "cn":fields.integer("CN",required=True),
                "cnps":fields.integer("CNPS",required=True),
                "nuit":fields.integer("NUIT",required=True),
                "total_retenues":fields.integer("Total Retenues",required=True),
                "avance":fields.integer("Avances & Accomptes",required=True),
                "indem_non_impo":fields.integer("Indemnité non impossable",required=True),
                "net_a_paye":fields.integer("Net à payer"),
                "hr_payroll_id":fields.many2one('hr.payroll',"Livre de paie",required=True),
            }
hr_payroll_line()
