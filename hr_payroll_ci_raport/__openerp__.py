{
    "name": "Les Rapports de paie",
    "version": "1.0",
    "depends": ["hr_payroll_ci"],
    "author": "Coulibaly Konzaga Apollinaire",
    "category": "hr",
    "description": """
    This module provide :
    """,
    "init_xml": [],
    'update_xml': [
                "data/report_paperformat.xml",
                "rapports/payroll_rapport.xml",
                "views/cnps_mensuel_report.xml",
                "views/report_payroll.xml",
                "hr_payroll_view.xml",
				"hr_cnps_monthly.xml",
            ],
    'demo_xml': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}