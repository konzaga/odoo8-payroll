# -*- encoding: utf-8 -*-


import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp.tools.translate import _

from openerp.osv import osv,fields


class hr_payroll_cnps_monthly(osv.osv):
    
    def get_list_bulletion_par_pedriode(self,cr,uid,ids,period_start,period_stop,categorie_id,context=None):
        payslip_obj=self.pool.get("hr.payslip")
        list=[]
        cr.execute("SELECT id FROM hr_payslip where (date_from between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))\
            and (date_to between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) order by id",(period_start,period_stop,period_start,period_stop))
        payslip_ids=cr.fetchall()
        if categorie_id :
            if payslip_ids :
                for payslip_id in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,payslip_id[0],context=context)
                    if payslip.contract_id.categorie_id==categorie_id :
                        list+=[payslip.id]
        else :
            if payslip_ids :
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id[0],context=context)
                    list+=[payslip.id]
        return list
    
    def get_list_employee_par_pedriode(self,cr,uid,ids,period_start,period_end,categorie_id,context=None):
        payslip_obj=self.pool.get("hr.payslip")
        list_payslip_ids=self.get_list_bulletion_par_pedriode(cr, uid, ids,period_start,period_end,categorie_id, context)
        list=[]
        for payslip_id in list_payslip_ids :
            payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
            list+=[payslip.employee_id.id]
        for i in list :
            trouve=0
            for l in list :
                if i == l :
                    trouve+=1
                    if trouve>1:
                        list.remove(l)
        return list
    
    #retourne la liste des bulletins à prendre en compte sur la période définie
    def get_list_bulletion_pedriode(self,cr,uid,ids,context=None):
        payslip_obj=self.pool.get("hr.payslip")
        list=[]
        for i in self.browse(cr, uid, ids, context=context):
            cr.execute("SELECT id FROM hr_payslip where (date_from between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))\
            and (date_to between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) order by id",(i.period_start,i.period_stop,i.period_start,i.period_stop))
            payslip_ids=cr.fetchall()
            if payslip_ids :
                for payslip_id in payslip_ids :
                        payslip=payslip_obj.browse(cr,uid,payslip_id[0],context=context)
                        if payslip.contract_id.type_id.code!='FONC':
                            list+=[payslip.id]
#                 else :
#                     for payslip_id in payslip_ids :
#                         payslip=payslip_obj.browse(cr,uid,payslip_id[0],context=context)
#                         if payslip.contract_id.categorie_id == i.categorie_contract_id and payslip.contract_id.type_id.type_contract!='fonctionnaire':
#                             list+=[payslip.id]
                
#         raise osv.except_osv('test',list)
        return list
    
    #retourne les ids des employées rémunérés sur la période
    def get_list_employee_pedriode(self,cr,uid,ids,context=None):
        payslip_obj=self.pool.get("hr.payslip")
        list_payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context)
        list=[]
        for i in self.browse(cr, uid, ids, context=context):
            for payslip_id in list_payslip_ids :
                payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                list+=[payslip.employee_id.id]
        for i in list :
            trouve=0
            for l in list :
                if i == l :
                    trouve+=1
                    if trouve>1:
                        list.remove(l)
        return list
    
    
    #la founction qui permet de retourner le nombre de journaliers dont le montant journalier est <=3231 F
    def get_nombre_journalier_first(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get("hr.payslip")
        list=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        list_employee=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        nbre=0
        for employee_id in list_employee:
            trouve=0
            for i in list :
                payslip=payslip_obj.browse(cr,uid,i,context=context)
                if employee_id == payslip.employee_id.id :
                    if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage<=3231:
                        trouve+=1
                        if trouve==1:
                            nbre+=1
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=nbre
        return res
    
    
     #la founction qui permet de retourner le nombre de journaliers dont le montant journalier est >3231 F
    def get_nombre_journalier_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get("hr.payslip")
        list=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        list_employee=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        nbre=0
        for employee_id in list_employee:
            trouve=0
            for i in list :
                payslip=payslip_obj.browse(cr,uid,i,context=context)
                if employee_id == payslip.employee_id.id :
                    if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage>3231:
                        trouve+=1
                        if trouve==1:
                            nbre+=1
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=nbre
        return res
        #raise osv.except_osv("Test",nbre)
        
        
    #la founction qui permet de retourner le nombre de mensuel dont le montant mensuel est >70000 F
    def get_nombre_mensuel_fisrt(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get("hr.payslip")
        list=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        list_employee=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        nbre=0
        for employee_id in list_employee:
            for i in list :
                payslip=payslip_obj.browse(cr,uid,i,context=context)
                if employee_id == payslip.employee_id.id :
                    if payslip.contract_id.type_id.code!="journalier" :
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.amount<=70000:
                                nbre+=1
        for i in self.browse(cr, uid, ids, context=context):
            #raise osv.except_osv("Test",nbre)
            res[i.id]=nbre
        return res
    
    #la founction qui permet de retourner le nombre de mensuel dont le montant mensuel est >70000 F
    def get_nombre_mensuel_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get("hr.payslip")
        list=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        list_employee=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        nbre=0
        for employee_id in list_employee:
            for i in list :
                payslip=payslip_obj.browse(cr,uid,i,context=context)
                if employee_id == payslip.employee_id.id :
                    if payslip.contract_id.type_id.code!="journalier" :
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total>70000 and line.total <=1647315:
                                nbre+=1
        for i in self.browse(cr, uid, ids, context=context):
            #raise osv.except_osv("Test",nbre)
            res[i.id]=nbre
        return res
    
    
    
    #la founction qui permet de retourner le nombre de mensuel dont le montant mensuel est >70000 F
    def get_nombre_mensuel_three(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get("hr.payslip")
        list=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        list_employee=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        nbre=0
        for employee_id in list_employee:
            for i in list :
                payslip=payslip_obj.browse(cr,uid,i,context=context)
                if employee_id == payslip.employee_id.id :
                    if payslip.contract_id.type_id.code!="journalier" :
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total>1647315:
                                nbre+=1
        for i in self.browse(cr, uid, ids, context=context):
            #raise osv.except_osv("Test",nbre)
            res[i.id]=nbre
        return res
    
        #raise osv.except_osv("Test",nbre)
            
    #la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 1        
    def get_montant_regime_journ_first(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage<=3231:
                            cr.execute("select total from hr_payslip_line where slip_id=%s and code='BASE_CNPS'"%payslip.id)
                            montant_employee+=cr.fetchone()[0]
                if montant_employee >1647315:
                    montant+=1647315
                if montant_employee <= 1647315 :
                    montant+=montant_employee
            res[cnps.id]=montant
        return res
    
    
    
    #la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 2        
    def get_montant_regime_journ_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage>3231:
                            cr.execute("select total from hr_payslip_line where slip_id=%s and code='BASE_CNPS'"%payslip.id)
                            montant_employee+=cr.fetchone()[0]
                if montant_employee >1647315:
                    montant+=1647315
                if montant_employee <= 1647315 :
                    montant+=montant_employee
            res[cnps.id]=montant
        return res



#la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 1        
    def get_montant_regime_mensuel_first(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="journalier" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total<=70000:
                                    montant_employee+=line.total
                                
                if montant_employee >1647315:
                    montant+=1647315
                if montant_employee <= 1647315 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
    
    
    
    
    #la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 1        
    def get_montant_regime_mensuel_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="journalier" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total>70000 and line.total<=1647315:
                                    montant_employee+=line.total
                                
                if montant_employee >1647315:
                    montant+=1647315
                if montant_employee <= 1647315 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
    
    
    
    #la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 1        
    def get_montant_regime_mensuel_third(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="journalier" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total>1647315 :
                                    montant_employee+=1647315
                                
                if montant_employee >1647315:
                    montant+=1647315
                if montant_employee <= 1647315 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
                        
    #la fonction qui permet de calculer le montant des regimes de prestations familiale pour la tranche 1
    def get_montant_prestation_first(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        #raise osv.except_osv("Test",payslip_ids)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage<=3231:
                            cr.execute("select total from hr_payslip_line where slip_id=%s and code='BASE_CNPS'"%payslip.id)
                            montant_employee+=cr.fetchone()[0]
                if montant_employee <= 70000 :
                    montant+=montant_employee
                if montant_employee >70000:
                    montant+=70000
                
            res[cnps.id]=montant
        return res
    
    
    #la fonction qui permet de calculer le montant des regimes de prestations familiale pour la tranche 2
    def get_montant_prestation_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code=="journalier" and payslip.contract_id.wage>3231:
                            cr.execute("select total from hr_payslip_line where slip_id=%s and code='BASE_CNPS'"%payslip.id)
                            montant_employees=cr.fetchone()
                            if montant_employees:
                                montant_employee+=montant_employees[0]
                            
                if montant_employee <= 70000 :
                    montant+=montant_employee
                if montant_employee >70000:
                    montant+=70000
                
            res[cnps.id]=montant
        return res
    
    
    #la fonction qui permet de retourner le montant du regime de retraite pour les journaliers de la tranche 1        
    def get_montant_prestation_mensuel_first(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="journalier" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total<=70000 :
                                    montant_employee+=line.total
                                
                if montant_employee >70000:
                    montant+=70000
                if montant_employee <= 70000 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
    def get_montant_prestation_mensuel_second(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="JOUR" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total>70000 and line.total<=1647315:
                                    montant_employee+=line.total
                                
                if montant_employee >70000:
                    montant+=70000
                if montant_employee <= 70000 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
    def get_montant_prestation_mensuel_third(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for cnps in self.browse(cr, uid, ids, context=context):
            montant=0
            for employee_id in employee_ids:
                montant_employee=0
                for i in payslip_ids :
                    payslip=payslip_obj.browse(cr,uid,i,context=context)
                    if employee_id == payslip.employee_id.id :
                        if payslip.contract_id.type_id.code!="journalier" :
                            for line in payslip.line_ids:
                                if line.code =="BRUT" and line.total>1647315 :
                                    montant_employee+=line.total
                                
                if montant_employee >70000:
                    montant+=70000
                if montant_employee <= 70000 :
                    montant+=montant_employee
            #raise osv.except_osv("Test",montant)
            res[cnps.id]=montant
        return res
    def _get_total_salaire_brut(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        #raise osv.except_osv("Test",payslip_ids)
        for i in self.browse(cr, uid, ids, context=context):
            montant=0
            for payslip_id in payslip_ids :
                payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                for line in payslip.line_ids:
                    if line.code=="BRUT":
                        montant+=line.amount     
            res[i.id]=montant
        return res
    def get_montant_journalier_tranche1(self,cr,uid,ids,context=None):
        res={}
        payslip_obj=self.pool.get('hr.payslip')
        cr.execute("select distinct contract.id from  hr_contract as contract, hr_contract_type as type_contract \
        where type_contract.code='JOUR' and contract.type_id=type_contract.id and contract.wage>3231")
        contract_ids=cr.fetchall()
        montant=0
        for i in self.browse(cr,uid,ids,context=context):
            payslip_ids=payslip_obj.search(cr,uid,[('date_from','>=',i.period_start),('date_to','<=',i.period_stop)],context=context)
            if payslip_ids!=[]:
                if contract_ids!=[]:
                    for contract_id in contract_ids:
                       for payslip_id in payslip_ids: 
                           payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                           if payslip.contract_id==contract_id[0]:
                               cr.execute("SELECT total hr_payslip_line WHERE slip_id=%s"%payslip_id[0])
                               raise osv.except_osv('TEst',cr.fetchone()[0])
    def _get_total_montant_famille(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr,uid,ids,context=context):
            res[i.id]=i.famille_journalier_tranche1+i.famille_journalier_tranche2+i.famille_montant_1+i.famille_montant_2+i.famille_montant_3
        return res
    def _get_total_montant_regime(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr,uid,ids,context=context):
            res[i.id]=i.regime_journalier_tranche1+i.regime_journalier_tranche2+i.regime_montant_1+i.regime_montant_2+i.regime_montant_3
        return res
    def _get_total_regime_famille(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=(i.taux_prestation*i.total_famille)/100
        return res
    def _get_total_accident_travail(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=(i.taux_accident_travail*i.total_famille)/100
        return res   
    def _get_total_regime_retraite(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=(i.taux_regime_retraite*i.total_regime)/100
        return res 
    def _get_total_cotisation(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            res[i.id]=i.total_prestation_famille+i.total_accident_travail+i.total_regime_retraite
        return res
    def _get_total_salarie(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            #raise osv.except_osv("resr",i.nbre_jrnlier_1 )
            res[i.id]=i.nbre_jrnlier_1 + i.nbre_jrnlier_2 + i.employee_classe_1 + i.employee_classe_2 + i.employee_classe_3
        return res
    
    def get_name(self,cr,uid,ids,field_name,arg,context=None):
        month={'01':'Janvier','02':'Février','03':'Mars','04':'Avril','05':'Mai','06':'Juin','07':'Juillet',
                   '08':'Aout','09':'Septembre','10':'Octobre','11':'Novembre','12':'Décembre'}
        res={}
        for i in self.browse(cr, uid, ids, context=context):
            mois=i.period_start[5:7]
            true_month=month[mois]
            res[i.id]=true_month
        return res
            
    _name="hr.payroll.cnps.monthly"
    _description="CNPS mensuel"
    _columns={
              "name":fields.char("Déclaration CNPS Mensuel",size=128,required=True),
              "month":fields.function(get_name,method=True,type='char',size=128,string='Mois'),
              'company_id':fields.many2one("res.company","Compagnie",required=True),
              #"categorie_contract_id":fields.many2one("hr.categorie.contract","Catégorie"),
              'period_start':fields.date("Date de début",required=True),
              'period_stop':fields.date("Date fin",required=True),
              'nbre_jrnlier_1':fields.function(get_nombre_journalier_first,method=True,type='integer',string="Nombre de journalier tranche 1",store=True),
              "nbre_jrnlier_2":fields.function(get_nombre_journalier_second,method=True,type='integer',string="Nombre de journalier tranche 2",store=True),
              "employee_classe_1":fields.function(get_nombre_mensuel_fisrt,method=True,type="integer",string="Employés classe 1",store=True),
              "employee_classe_2":fields.function(get_nombre_mensuel_second,method=True,type="integer",string="Employés classe 2",store=True),
              "employee_classe_3":fields.function(get_nombre_mensuel_three,method=True,type="integer",string="Employés classe 3",store=True),
              "regime_journalier_tranche1":fields.integer(),
              "regime_journalier_tranche2":fields.integer(),
              "regime_montant_1":fields.integer(),
              "regime_montant_2":fields.integer(),
              "regime_montant_3":fields.integer(),
              "famille_journalier_tranche1":fields.integer(),
              "famille_journalier_tranche2":fields.integer(),
              "famille_montant_1":fields.integer(),
              "famille_montant_2":fields.integer(),
              "famille_montant_3":fields.integer(),
              "montant_famile_paye":fields.integer("Montant prestations familiale"),
              "montant_accident_travail":fields.integer("Montant accident de travail"),
              "montant_regime_retraite":fields.integer("Montant regime de ratraite"),
              "total_famille":fields.integer(),
              "total_regime":fields.integer(),
              #"variable_ids":fields.many2many("hr.variable.cnps","cnps_variable_reel","cnps_monthly_id","variable_id","Variables"),
              "taux_prestation":fields.float(),
              "taux_accident_travail":fields.float(),
              "taux_regime_retraite":fields.float(),
              "total_employee":fields.function(_get_total_salarie,method=True,type='integer',string="Total employés",store=True),
              "total_prestation_famille":fields.function(_get_total_regime_famille,method=True,type='integer',string="Montant total prestation de famille",store=True),
              "total_accident_travail":fields.function(_get_total_accident_travail,method=True,type='integer',string="Montant total prestation de famille",store=True),
              "total_regime_retraite":fields.function(_get_total_regime_retraite,method=True,type='integer',string="Montant total regime de ratraite",store=True),
              "total_cotisation":fields.function(_get_total_cotisation,method=True,type='integer',string="TotalCotisation",store=True),
              "total_salaire_brut":fields.function(_get_total_salaire_brut,method=True,type="integer",store=True),
              "cnps_trimestriel_id":fields.many2one("hr.cnps.trimestrial",'CNPS TRIMESTRIEL'),
              }
    _defaults={
               'period_start': lambda *a: time.strftime('%Y-%m-01'),
               'period_stop': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
               'famille_journalier_tranche2':0,
               'company_id':1,
               }
    
    def get_taux_cnps(self,cr,uid,ids,context=None):
        res={"taux_prestation":0,
            'taux_accident_travail':0,
            "taux_regime_retraite":0,
            }
        cr.execute("SELECT amount_percentage FROM hr_salary_rule WHERE code='PF'")
        taux_prestation=cr.fetchone()[0]
        cr.execute("SELECT amount_percentage FROM hr_salary_rule WHERE code='ACT'")
        taux_accident=cr.fetchone()[0]         
        cr.execute("SELECT amount_percentage FROM hr_salary_rule WHERE code='RETR'")
        taux_regime=cr.fetchone()[0]      
        res.update({"taux_prestation":taux_prestation,
            'taux_accident_travail':taux_accident,
            "taux_regime_retraite":taux_regime,
            })
        return res
    def get_journalier_tranche1(self,cr,uid,ids,context=None):
        res={"regime_journalier_tranche1":0,
            'famille_journalier_tranche1':0,}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for i in self.browse(cr, uid, ids, context=context):
            total_prestation=0
            total_regime=0
            for employee_id in employee_ids:
                prestation_famille=0
                regime_retraite=0
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                    if employee_id == payslip.employee_id.id and payslip.contract_id.type_id.code=='JOUR' and payslip.contract_id.wage<=3231:
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total<=70000:
                                prestation_famille+=line.total
                                regime_retraite+=line.total
                total_prestation+=min(70000,prestation_famille)
                total_regime+=min(regime_retraite,1647315)
            res.update({
                        "regime_journalier_tranche1":total_regime,
                        "famille_journalier_tranche1":total_prestation,
                                 })
        return res
    def get_journalier_tranche2(self,cr,uid,ids,context=None):
        res={"regime_journalier_tranche2":0,
            'famille_journalier_tranche2':0,}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for i in self.browse(cr, uid, ids, context=context):
            total_prestation=0
            total_regime=0
            for employee_id in employee_ids:
                prestation_famille=0
                regime_retraite=0
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                    if employee_id == payslip.employee_id.id and payslip.contract_id.type_id.code=='JOUR' and payslip.contract_id.wage>3231:
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total<=70000:
                                prestation_famille+=line.total
                                regime_retraite+=line.total
                total_prestation+=min(70000,prestation_famille)
                total_regime+=min(regime_retraite,1647315)
            res.update({
                        "regime_journalier_tranche2":total_regime,
                        "famille_journalier_tranche2":total_prestation,
                                 })
            #raise osv.except_osv("Test jopurnalier",res)
        return res
    def get_all_tranche1(self,cr,uid,ids,context=None):
        res={"regime_montant_1":0,
            'famille_montant_1':0,}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for i in self.browse(cr, uid, ids, context=context):
            total_prestation=0
            total_regime=0
            for employee_id in employee_ids:
                prestation_famille=0
                regime_retraite=0
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                    if employee_id == payslip.employee_id.id and payslip.contract_id.type_id.code!='JOUR':
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total<=70000:
                                prestation_famille+=line.total
                                regime_retraite+=line.total
                total_prestation+=min(70000,prestation_famille)
                total_regime+=min(regime_retraite,1647315)
            res.update({
                        "regime_montant_1":total_regime,
                        "famille_montant_1":total_prestation,
                                 })
        return res
    def get_all_tranche2(self,cr,uid,ids,context=None):
        res={
             "regime_montant_2":0,
             'famille_montant_2':0,}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for i in self.browse(cr, uid, ids, context=context):
            total_prestation=0
            total_regime=0
            for employee_id in employee_ids:
                prestation_famille=0
                regime_retraite=0
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                    if employee_id == payslip.employee_id.id and payslip.contract_id.type_id.code!='JOUR':
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total>70000 and line.total<=1647315:
                                prestation_famille+=line.total
                                regime_retraite+=line.total
                total_prestation+=min(70000,prestation_famille)
                total_regime+=min(regime_retraite,1647315)
            res.update({
                        "regime_montant_2":total_regime,
                        "famille_montant_2":total_prestation,
                    })
        return res
    def get_all_tranche3(self,cr,uid,ids,context=None):
        res={
                      "regime_montant_3":0,
                      'famille_montant_3':0,}
        payslip_obj=self.pool.get('hr.payslip')
        payslip_ids=self.get_list_bulletion_pedriode(cr, uid, ids, context=context)
        employee_ids=self.get_list_employee_pedriode(cr, uid, ids, context=context)
        for i in self.browse(cr, uid, ids, context=context):
            total_prestation=0
            total_regime=0
            for employee_id in employee_ids:
                prestation_famille=0
                regime_retraite=0
                for payslip_id in payslip_ids:
                    payslip=payslip_obj.browse(cr,uid,payslip_id,context=context)
                    if employee_id == payslip.employee_id.id and payslip.contract_id.type_id.code!='JOUR':
                        for line in payslip.line_ids:
                            if line.code=="BRUT" and line.total>1647315:
                                ##raise osv.except_osv("Test looool","trouver")
                                prestation_famille+=line.total
                                regime_retraite+=line.total
                total_prestation+=min(70000,prestation_famille)
                total_regime+=min(regime_retraite,1647315)
            res.update({
                                 "regime_montant_3":total_regime,
                                 "famille_montant_3":total_prestation,
                                 })
            return res
        
    def result(self,cr,uid,ids,period_start,period_end,context=None):
        raise osv.except_osv('Test','Test')
    
    def write(self,cr,uid,ids,vals,context=None):
        result={'value':{}}
        res=self.get_all_tranche3(cr, uid, ids, context)
        res.update(self.get_all_tranche2(cr, uid, ids, context))
        res.update(self.get_all_tranche1(cr, uid, ids, context))
        res.update(self.get_journalier_tranche1(cr, uid, ids, context))
        res.update(self.get_journalier_tranche2(cr, uid, ids, context))
        res.update(self.get_taux_cnps(cr, uid, ids, context))
        total_famille=res['famille_journalier_tranche1']+res['famille_journalier_tranche2']+res['famille_montant_1']+res['famille_montant_2']+res['famille_montant_3']
        total_regime=res['regime_journalier_tranche1']+res['regime_journalier_tranche2']+res['regime_montant_1']+res['regime_montant_2']+res['regime_montant_3']
        vals=res
        vals['total_famille']=total_famille
        vals['total_regime']=total_regime
        total_prestation_famille=int((vals['total_famille'])*(vals['taux_prestation'])/100)
        total_accident_travail=int((vals['total_famille'])*(vals['taux_accident_travail'])/100)
        total_regime=int((vals['total_regime'])*(vals['taux_regime_retraite'])/100)
        vals['total_prestation_famille']=total_prestation_famille
        vals['total_accident_travail']=total_accident_travail
        vals['total_regime_retraite']=total_regime
        #raise osv.except_osv("Total famille",vals)
        return super(hr_payroll_cnps_monthly,self).write(cr,uid,ids,vals,context=context)
    
    def print_cnps_mensuel(self,cr,uid,ids,context=None):    
        result={'value':{}}
        res=self.get_all_tranche3(cr, uid, ids, context)
        res.update(self.get_all_tranche2(cr, uid, ids, context))
        res.update(self.get_all_tranche1(cr, uid, ids, context))
        res.update(self.get_journalier_tranche1(cr, uid, ids, context))
        res.update(self.get_journalier_tranche2(cr, uid, ids, context))
        res.update(self.get_taux_cnps(cr, uid, ids, context))
        total_famille=res['famille_journalier_tranche1']+res['famille_journalier_tranche2']+res['famille_montant_1']+res['famille_montant_2']+res['famille_montant_3']
        total_regime=res['regime_journalier_tranche1']+res['regime_journalier_tranche2']+res['regime_montant_1']+res['regime_montant_2']+res['regime_montant_3']
        vals=res
        vals['total_famille']=total_famille
        vals['total_regime']=total_regime
        total_prestation_famille=int((vals['total_famille'])*(vals['taux_prestation'])/100)
        total_accident_travail=int((vals['total_famille'])*(vals['taux_accident_travail'])/100)
        total_regime=int((vals['total_regime'])*(vals['taux_regime_retraite'])/100)
        vals['total_prestation_famille']=total_prestation_famille
        vals['total_accident_travail']=total_accident_travail
        vals['total_regime_retraite']=total_regime
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'hr_payroll_ci_raport.cnps_mensuel_report', context=context)
    
    
    
hr_payroll_cnps_monthly()



              