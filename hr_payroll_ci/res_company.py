# -*- coding: utf-8 -*-
##############################################################################
##############################################################################

import datetime
import math
import time
from operator import attrgetter

from openerp.exceptions import Warning
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _

class res_company(osv.osv):
    _inherit = 'res.company'
    _columns = {
                'num_cnps':fields.char("Numéro CNPS", size=124,required=True),
                'num_contribuable':fields.char("Numéro Contribuable",size=128,required=True),
            } 
res_company()
    #Do not touch _name it must be same as _inherit
    #_name = 'res.company'