##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_synthese
# ##############################################################################
{
    "name" : "Payroll Côte d'Ivoire",
    "version" : "1.0",
    "author" : "VEONE Technologies",
    'category': 'Localization',
    "website" : "www.veone.net",
    "depends" : ["report","hr_payroll","hr_contract_extension","hr_emprunt"],
    "description": """ Synthèse de la paie
    - livre de paie mensuelle et périodique
    - Synthèse de paie des employés
    - interfaçage avec la gestion des contrats des employés
    """,
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
                    "security/hr_security.xml",
                    "security/ir.model.access.csv",
                    "hr_payroll_report.xml",
                    "wizards/hr_payroll_inverse_view.xml",
                    "hr_group.xml",
                    "res_company_view.xml",
                    "report_payslip.xml",
                    "hr_payroll_ci.xml",
                    ],
    "data":[
            'data/hr_salary_rule_category.xml',
            'data/hr_salary_rule.xml',
            'data/hr_rule_input.xml',
            'data/hr_payroll_structure.xml',
            #'data/hr_structure_salary_rule_rel.xml',
            ],
    "installable": True
}
