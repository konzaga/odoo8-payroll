# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2014 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_payroll_ci
# ##############################################################################  -->


import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import netsvc
from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
from openerp.exceptions import Warning

from openerp.tools.safe_eval import safe_eval as eval
from decimal import Decimal
from collections import namedtuple
from math import fabs, ceil, floor

import openerp.addons.decimal_precision as dp



class hr_payslip(osv.osv):
    _inherit="hr.payslip"
       
    def get_days_periode(self,start, end):
        r = (end + timedelta(days=1) - start).days
        return [start+timedelta(days=i) for i in range(r)]
    
    def write(self, cr, user, ids, vals, context=None):  
        payslips = self.browse(cr, user, ids, context=context)
        trouver = False
        for payslip in payslips:
            employee=self.pool.get('hr.employee').browse(cr, user, payslip.employee_id.id, context=context)
            list_payslips=employee.slip_ids
            date_from = datetime.strptime(payslip.date_from,'%Y-%m-%d')
            date_to = datetime.strptime(payslip.date_to,'%Y-%m-%d')
            Range = namedtuple('Range',['start','end'])
            r1=Range(start=date_from,end=date_to)
            new_list=[]
            if (len(list_payslips)!=1):
                for slip in list_payslips:
                    if slip.id != payslip.id :
                        new_list.append(slip)
                for slip in new_list:
                    old_date_from=datetime.strptime(slip.date_from,'%Y-%m-%d')
                    old_date_to = datetime.strptime(slip.date_to,'%Y-%m-%d')
                    r2=Range(start=old_date_from,end=old_date_to)
                    result = (min(r1.end, r2.end)  - max(r1.start,r2.start)).days + 1
                    if result > 0 and slip.contract_id.categorie_id == payslip.contract_id.categorie_id: 
                        trouver = True 
            if trouver == True :
                raise osv.except_osv(_('Warning'),_("L'employé possède déjà un bulletin pour cette période"))                                  
            else :
                super(hr_payslip,self).write(cr, user, ids, vals, context)
        return True
         
    def get_test(self,cr, uid,ids,employee_id, context=None):
        res = {}
        obj_payslip = self.pool.get('hr.payslip')
        for emp in self.browse(cr, uid, ids, context=context):
            contract_ids = obj_payslip.search(cr, uid, [('employee_id','=',emp.id),], context=context)
            if contract_ids:
                raise osv.except_osv(_("test"),_("test"))     
        
    def get_emprunt_montant_monthly(self,cr,uid,ids,employee_id,date_from, date_to,context=None):
        cp=cr
        cp.execute("SELECT * FROM hr_emprunt as emp, hr_echeance as ech WHERE emp.employee_id=%s and\
        ech.date_remboursement_echeance<=%s or ech.date_remboursement_echeance>=%s\
        and ech.emprunt_id=emp.id",(employee_id,date_from,date_to))
        test=cp.fetchall()
        raise osv.except_osv(_('Test'),test)
        
    
    #def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
     #   """
      #  @param contract_ids: list of contract id
      #  @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
      #  """
      #  hstatus_obj=self.pool.get('hr.holidays.status')
      #  def was_on_leave(employee_id, datetime_day, context=None):
     #       res = False
      #      day = datetime_day.strftime("%Y-%m-%d")
      #      holiday_ids = self.pool.get('hr.holidays').search(cr, uid, [('state','=','validate'),('employee_id','=',employee_id),('type','=','remove'),('date_from','<=',day),('date_to','>=',day)])
     #       if holiday_ids:
      #          res = self.pool.get('hr.holidays').browse(cr, uid, holiday_ids, context=context)[0].holiday_status_id.name
       #     return res
#
 #       res = []
  #      
   #     for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
    #        if not contract.working_hours:
                #fill only if the contract as a working schedule linked
    #            continue
    #        nbre_days=(173.33/8)
   #         montant = Decimal(str(round(nbre_days,0)))
    #        attendances = {
     #            'name': _("Normal Working Days paid at 100%"),
     #            'sequence': 1,
     #            'code': 'WORK100',
     #            'number_of_days': float(montant),
     #            'number_of_hours': 173.33,
     #            'contract_id': contract.id,
     #       }
     #       leaves = {}
     #       day_from = datetime.strptime(date_from,"%Y-%m-%d")
     #       day_to = datetime.strptime(date_to,"%Y-%m-%d")
     #       nb_of_days = (day_to - day_from).days + 1
     #       cr.execute("select distinct holiday_status_id,sum(number_of_days_temp),sum(number_of_hours) from hr_holidays where type='remove'" 
     #               " and state='validate' and (date_from between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) "
     #               " and (date_to between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) and employee_id=%s group by holiday_status_id",(date_from, date_to,date_from, date_to,
     #                                                                                                                                             contract.employee_id.id))
     #       results=cr.fetchall()
     #       if results :
     #           for result in results :
     #               hstatus=hstatus_obj.browse(cr,uid,result[0],context)
     #               if hstatus :
     #                   nbre_days=(result[2]/8)
     #                   nbre = Decimal(str(round(nbre_days,0)))
     #                   leaves[hstatus.name] = {
     #                       'name': hstatus.name,
     #                       'sequence': 5,
     #                       'code': hstatus.code,
     #                       'number_of_days': float(nbre),
     #                       'number_of_hours': result[2],
     #                       'contract_id': contract.id,
     #                   }
     #                   attendances['number_of_days'] -= leaves[hstatus.name]['number_of_days']
     #                   attendances['number_of_hours'] -= result[2]
     #       leaves = [value for key,value in leaves.items()]
     #       res += [attendances] + leaves
     #   return res
    
    def get_inputs(self, cr, uid, contract_ids, date_from, date_to, context=None):
        res = super(hr_payslip,self).get_inputs(cr,uid,contract_ids,date_from,date_to,context=context)
        for payslip in self.browse(cr,uid,contract_ids,context=context):
            contract=self.pool.get('hr.contract').browse(cr,uid,payslip.id,context=context)
            for prime in contract.hr_payroll_prime_ids:
                if prime.prime_id.code == "TRSP" :
                    if prime.montant_prime > 25000 :
                        montant_imp = prime.montant_prime - 25000
                        inputs={
                                'name': "Prime de transport imposable",
                                'code': "TRSP_IMP",
                                'contract_id': contract.id,
                                'amount':montant_imp,
                            }
                        res+=[inputs]
                        inputs={
                            'name': prime.prime_id.name,
                            'code': prime.code,
                            'contract_id': contract.id,
                            'amount':25000,
                        }
                        res+=[inputs]  
                    else :
                        inputs={
                            'name': prime.prime_id.name,
                            'code': prime.code,
                            'contract_id': contract.id,
                            'amount':prime.montant_prime,
                            }
                        res+=[inputs]    
                else :
                    inputs={
                            'name': prime.prime_id.name,
                            'code': prime.code,
                            'contract_id': contract.id,
                            'amount':prime.montant_prime,
                        }
                    res+=[inputs]    
            liste=[]
            montant=0
            if contract.employee_id.emprunt_ids:
                for empr in contract.employee_id.emprunt_ids :
                    echeances = empr.echeance_ids
                    for ech in echeances:
                        if date_from<=ech.date_prevu and ech.date_prevu<=date_to and ech.statut_echeance is False:
                            liste+=[ech]
                for el in liste:
                    montant = montant + el['montant_echeance']
                inputs={
                            'name':'Emprunt à deduire',
                            'code':'EMP',
                            'contract_id':contract.id,
                            'amount':montant,
                        }  
                res +=[inputs]   
                inputs={
                        'name':'Autres Indemnités non imposable',
                        'code':'AUTR_NON_IMP',
                        'contract_id':contract.id,
                        'amount':0,
                    }  
                res +=[inputs]                
        return res    
    
    # la nouvelle fonction surchargée
        
    def get_payslip_lines(self, cr, uid, contract_ids, payslip_id, context):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
            return localdict

        
        class BrowsableObject(object):
            def __init__(self, pool, cr, uid, employee_id, dict):
                self.pool = pool
                self.cr = cr
                self.uid = uid
                self.employee_id = employee_id
                self.dict = dict

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(amount) as sum\
                            FROM hr_payslip as hp, hr_payslip_input as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()[0]
                return res or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours\
                            FROM hr_payslip as hp, hr_payslip_worked_days as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done'\
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                return self.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                self.cr.execute("SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s",
                            (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()
                return res and res[0] or 0.0
            
            
            def arrondi(self,montant):
                montant = Decimal(str(round(montant,0)))
                return float(montant)

            def arrondimilinf(self,montant):
                montant = Decimal(str(floor(montant / 1000)))
                return float(montant * 1000)
            
            def get_cumul_rubrique(self,code):
                if self.last_payslip == False :
                    return 0.0
                else :
                    annee=datetime.now().year
                    last_year=str(annee - 1)
                    if self.last_payslip.date_from[2:4]==last_year or self.last_payslip.date_to[2:4]==last_year :
                        return 0.0
                    else :
                        self.cr.execute("SELECT pl.total\
                                FROM hr_payslip as hp, hr_payslip_line as pl \
                                WHERE hp.id = %s AND pl.slip_id=hp.id AND pl.code = %s",
                                (self.last_payslip.id, code))
                        res=self.cr.fetchone()
                        return res[0]
    
            def last_amount(self,code): 
                if self.last_payslip == False :
                    return 0.0                  
                else :
                    self.cr.execute("SELECT pl.amount\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.id = %s AND pl.slip_id=hp.id AND pl.code = %s",
                            (self.last_payslip.id, code))
                    res=self.cr.fetchone()
                    return res[0]

               
                   
        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules = {}
        categories_dict = {}
        blacklist = []
        payslip_obj = self.pool.get('hr.payslip')
        inputs_obj = self.pool.get('hr.payslip.worked_days')
        obj_rule = self.pool.get('hr.salary.rule')
        payslip = payslip_obj.browse(cr, uid, payslip_id, context=context)
        worked_days = {}
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days[worked_days_line.code] = worked_days_line
        inputs = {}
        for input_line in payslip.input_line_ids:
            inputs[input_line.code] = input_line

        categories_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, categories_dict)
        input_obj = InputLine(self.pool, cr, uid, payslip.employee_id.id, inputs)
        worked_days_obj = WorkedDays(self.pool, cr, uid, payslip.employee_id.id, worked_days)
        payslip_obj = Payslips(self.pool, cr, uid, payslip.employee_id.id, payslip)
        rules_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, rules)

        localdict = {'categories': categories_obj, 'rules': rules_obj, 'payslip': payslip_obj, 'worked_days': worked_days_obj, 'inputs': input_obj}
        #get the ids of the structures on the contracts and their parent id as well
        structure_ids = self.pool.get('hr.contract').get_all_structures(cr, uid, contract_ids, context=context)
        #get the rules of the structure and thier children
        rule_ids = self.pool.get('hr.payroll.structure').get_all_rules(cr, uid, structure_ids, context=context)
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]

        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            employee = contract.employee_id
            localdict.update({'employee': employee, 'contract': contract})
            for rule in obj_rule.browse(cr, uid, sorted_rule_ids, context=context):
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                #check if the rule can be applied
                if obj_rule.satisfy_condition(cr, uid, rule.id, localdict, context=context) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = obj_rule.compute_rule(cr, uid, rule.id, localdict, context=context)
                    #check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'group_id': rule.group_id.id,
                        'group_sequence': rule.group_id.sequence,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in self.pool.get('hr.salary.rule')._recursive_search_of_rules(cr, uid, [rule], context=context)]

        result = [value for code, value in result_dict.items()]
        return result
    

    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        hour=[]
        hour = super(hr_payslip,self).onchange_employee_id(cr, uid, ids, date_from, date_to, employee_id, contract_id, context=context)
        employee_obj=self.pool.get('hr.employee')
        if employee_id != False :
            employee = employee_obj.browse(cr,uid,employee_id,context=context)
            hour['value'].update({
                                  'payment_method':employee.payment_method,
                                  })
        return hour
    
             
    def calcul_anciennete_actuel(self,cr,uid,ids,context=None):
        payslips=self.browse(cr, uid, ids, context=context)
        anciennete={}
        for payslip in payslips:
            date_debut=datetime.strptime(payslip.contract_id.date_start,'%Y-%m-%d')
            date_fin = datetime.strptime(payslip.date_to,'%Y-%m-%d')
            result=date_fin - date_debut
            nombre_day=result.days
            nbre_year=int(nombre_day/360)
            nbre_year=payslip.contract_id.an_report+nbre_year
            jour_restant=result.days%360
            nbre_mois=jour_restant/30
            nbre_mois=nbre_mois+payslip.contract_id.mois_report
            if nbre_mois >= 12:
                nbre_year=nbre_year+int(nbre_mois/12)
                nbre_mois=nbre_mois%12
            anciennete={
                        'year_old':nbre_year,
                        'month_old':nbre_mois,
                        }
        return anciennete
    
    def get_anciennete(self,cr,uid,ids,date_start,context=None):
        res = {'value':{
                      'anne_anc':0,
                      }
            }
        payslips=self.browse(cr, uid, ids, context=context)
        for payslip in payslips:
            date_debut=datetime.strptime(payslip.contract_id.date_start,'%Y-%m-%d')
            today = datetime.strptime(payslip.date_to,'%Y-%m-%d')
            result=today-date_debut
            nombre_day=result.days
            nbre_year=int(nombre_day/360)
            nbre_year=payslip.contract_id.an_report+nbre_year
            jour_restant=result.days%360
            nbre_mois=jour_restant/30
            nbre_mois=nbre_mois+payslip.contract_id.mois_report
            if nbre_mois >= 12:
                nbre_year=nbre_year+int(nbre_mois/12)
                nbre_mois=nbre_mois%12
            res['value'].update({
                             'payslip_an_anciennete':nbre_year,
                             'payslip_mois_anciennete':nbre_mois,
                             })
        return res
    
    def _get_annee_actuel(self,cr,uid,ids,field,arg,context=None):
        res={}
        payslips=self.browse(cr,uid,ids,context)
        anciennete=self.calcul_anciennete_actuel(cr, uid,ids, context=context)
        for payslip in payslips :
                res[payslip.id] = anciennete['year_old']
        return res
    
    def _get_mois_actuel(self,cr,uid,ids,field,arg,context=None):
        res={}
        payslips=self.browse(cr,uid,ids,context)
        anciennete=self.calcul_anciennete_actuel(cr, uid,ids, context=context)
        for payslip in payslips :
                res[payslip.id] = anciennete['month_old']
        return res
    
    def _get_last_payslip(self,cr,uid,ids,field,arg,context=None):
        dic={}
        res=[]
        payslips=self.browse(cr, uid, ids, context=context)
        for payslip in payslips:
            slips = payslip.employee_id.slip_ids
            if (len(slips)==1 ) :
                dic[payslip.id] = False
            else :
                for slip in slips:
                    if (slip.id < payslip.id) :
                        res.append(slip)
                        if len(res)>= 1 :
                            dernier=res[len(res)-1]
                            payslip_obj=self.pool.get('hr.payslip').search(cr, uid, [('id','=',dernier.id)], context=context)
                            dic[payslip.id]=payslip_obj      
        return dic
    
    def _get_total_gain(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        #line_obj=self.pool.get("hr.payslip.line")
        for i in self.browse(cr, uid, ids, context=context):
            for line in i.line_ids :
                if line.code=='BRUT':
                    res[i.id]=line.total
        return res
    
    def _get_retenues(self,cr,uid,ids,fields_name,arg,context=None):
        res={}
        #line_obj=self.pool.get('hr.payslip.line')
        for i in self.browse(cr, uid, ids, context=context):
            for line in i.line_ids :
                if line.code=='RET':
                    res[i.id]=line.total
                    
        return res
    
    def _get_net_paye(self,cr,uid,ids,fields_name, arg, context=None):
        res={}
        #line_obj=self.pool.get("hr.payslip.line")
        for i in self.browse(cr, uid, ids, context=context):
            for line in i.line_ids :
                if line.code=="NET" :
                    res[i.id]=line.total
        return res
    
    def _get_last_arrondi(self,cr,uid,ids,field,arg,context=None):
        res = {}
        montant=0
        for payslip in self.browse(cr, uid, ids, context=context): 
            liste_line=payslip.last_payslip.line_ids
            if (liste_line) :
                for line in liste_line:
                    if line.salary_rule_id.code == 'AEP':
                        montant=line.amount
                        res[payslip.id]=montant
            else : 
                res[payslip.id] = 0
        return res
    
    def _get_actuel_id(self,cr,uid,ids,field,arg,context=None):
        res={}
        for payslip in self.browse(cr, uid, ids, context=context):
            res[payslip.id]=1
        return res
    
    _columns={
              'option_salaire':fields.selection([('mois','Mensuel'),
                                                 ('jour','Journalier'),
                                                 ('heure','horaire'),
                                                 ],'Option salaire', select=True, readonly=False),
            'reference_reglement' : fields.char('Reférence',size=60,required=False),
            'payslip_an_anciennete':fields.function(_get_annee_actuel,method=True, type='integer', 
                                              store=True,string="Nombre d'année"),
            'payslip_mois_anciennete':fields.function(_get_mois_actuel,method=True, type='integer', 
                                                store=True,string="Nombre de mois"),
            #'payment_method':fields.selection([('espece','Espèces'),('virement','Virement bancaire'),('cheque','Chèques')],
             #                                 string='Moyens de paiement',required=True),
            'last_payslip':fields.function(_get_last_payslip,method=True,type='many2one',relation="hr.payslip",store=True,string="Dernier bulletin"),
            'total_gain':fields.function(_get_total_gain,method=True,type='integer',store=True),
            'total_retenues':fields.function(_get_retenues,method=True,type='integer',store=True),
            'net_paie':fields.function(_get_net_paye,method=True,type='integer',store=True),
		
            } 
    
    
    def get_list_employee(self,cr,uid,ids,date_to,context=None): 
        list_employees=[]
        hc_obj=self.pool.get('hr.contract')
        hcontract_ids=hc_obj.search(cr,uid,[('state','=','in_progress')])
        cr.execute("SELECT employee_id FROM hr_contract WHERE id=ANY(%s)", (hcontract_ids,))
        results=cr.fetchall()
        if results :
             list_employees=[res[0] for res in results]
             return {'domain':{'employee_id':[('id','in',list_employees)]}}
			 
			 
			 
   # def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
   #     if employee_id :
   #         return {'domain':{'identification_id':[('employee_id','=',employee_id)]}}
    #    else :
    #        return {'domain':{'identification_id':[('employee_id','=',False)]}}


  #  def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id, contract_id, context=None):
   #     hour=[]
   #     employee_obj=self.pool.get('hr.employee')
    #    if employee_id != False :
     #       employee = employee_obj.browse(cr,uid,employee_id,context=context)
     #       hour['value'].update({
      #                            'payment_method':employee.payment_method,
      #                            })
      #  return hour
			
    #def onchange_identification_id(self, cr, uid, ids,identification_id, context=None):
    #    if identification_id :
    #        return {'domain':{'employee_id':[('identification_id','=',identification_id)]}}
     #   else :
     #       return {'domain':{'employee_id':[('identification_id','=',False)]}}
    def get_net_paye(self):
        montant = 0
        #line_obj=self.pool.get("hr.payslip.line")
        for line in self.line_ids :
                if line.code=="NET" :
                    montant = line.total
                    #print 'Montant du net %s'%montant
        return montant
        
             
             
hr_payslip()

class hr_payslip_line(osv.osv):
    '''
    Payslip Line
    '''

    _name = 'hr.payslip.line'
    _inherit = 'hr.payslip.line'
    
    def _calculate_total(self, cr, uid, ids, name, args, context):
        if not ids: return {}
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = float(line.quantity) * line.amount * line.rate / 100
        return res

    _columns = {
        'group_id' : fields.many2one('hr.salary.rule.group', 'Group', groups="base.group_user"),
        'group_sequence': fields.integer('Sequence group', required=False, select=True),
        'amount': fields.float('Amount', digits_compute=dp.get_precision('Payroll')),
        'quantity': fields.float('Quantity', digits_compute=dp.get_precision('Payroll')),
        'total': fields.function(_calculate_total, method=True, type='float', string='Total', digits_compute=dp.get_precision('Payroll'),store=True ),
    }
    
hr_payslip_line()



class hr_salary_rule(osv.osv):
    _inherit='hr.salary.rule'
    _order='sequence'
    _columns = { 
        'group_id' : fields.many2one('hr.salary.rule.group', 'Group',groups="base.group_user"),
 }
    def __init__(self,pool,cr):
        super(hr_salary_rule,self).__init__(pool,cr)
        cr.execute('DELETE FROM hr_salary_rule WHERE id in (1,2,3)')
        cr.commit()
        
    
hr_salary_rule()

class hr_salary_rule_group(osv.osv):

#    HR Salary Rule Group

    _name = 'hr.salary.rule.group'
    _description = 'Salary Rule Group'
    _order = 'sequence'
    _columns = {
        'name':fields.char('Name', required=True,),
        'sequence': fields.integer('Sequence', required=True, select=True),
        'note': fields.text('Description'),
        
    }
hr_salary_rule_group()
