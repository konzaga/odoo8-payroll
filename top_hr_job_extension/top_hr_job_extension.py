# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2019 top1 africa
# Author: top1
#
# 
# ##############################################################################  -->
from openerp.osv import fields, osv
import datetime
import time

class hr_job(osv.osv):
    _inherit  = "hr.job" 

		
    _columns={
        'famille_id' : fields.many2one("hr.job.family","Famille",required=False),
        'sous_famille_id' : fields.many2one('hr.job.sfamily',"Sous famille",required=False),
        'tranche_id' : fields.many2one("hr.job.tranche",string='Tranche catégorielle'),
        'direction_id' : fields.many2one("hr.job.direction","Direction",required=False),
        'departement_id' : fields.many2one("hr.department","Département", required=False),
		'service_id' : fields.many2one("hr.job.service","Service",required=False),
        'supN_1' : fields.many2one("hr.job","Supérieur Hiérarchique N+1",required=False),
        'supN_2' : fields.many2one("hr.job","Supérieur Hiérarchique N+2",required=False),
        'personne_encardre':fields.integer(string="Personne(s) encadrée (s)"),
        'interim':fields.many2one("hr.job","Intérim",required=False),
		'mission_globale_ids' : fields.one2many("hr.job.employe.mission",'job_id',srting="mission"),
		'mission_secondaire_ids' : fields.one2many("hr.job.employe.mission2",'job_id',srting=""),
        'taches_ids': fields.one2many("hr.job.employe.tasks","job_id",srting=""),
        'autre_taches_ids': fields.one2many("hr.job.employe.tasks2","job_id",srting=""),
        'taches_respon_ids': fields.one2many("hr.job.employe.tasks3","job_id",srting=""),
        'champ_autonomie':fields.selection([('very great','Très Grand'),('great','Grand'),
                                  ('moyen','Moyen'),
                                  ('low','Faible'),
                                  ('tres faible','Très Faible')],srting="Champ d’autonomie d'action ",required=False), 
		'impact':fields.text("IMPACT ",size=500,required=False),
		'kpi_ids' : fields.one2many("hr.job.kpi",'job_id',srting=""),
		'school_ids' : fields.one2many("hr.job.school",'job_id',srting=""),
        'experience_ids' : fields.one2many("hr.job.experience",'job_id',srting=""),
		'competence_ids' : fields.one2many("hr.job.competence",'job_id',srting=""),
		'competence1_ids' : fields.one2many("hr.job.competence1",'job_id',srting=""),
		'competence2_ids' : fields.one2many("hr.job.competence2",'job_id',srting=""),
		'competence3_ids' : fields.one2many("hr.job.competence3",'job_id',srting=""),
		'generale_ids' : fields.one2many("hr.job.generale.condition",'job_id',srting=""),
		'invisible' : fields.integer(srting="invisible"),
		
		
              }
    
   
    
    _defaults = {
        
    }
				
hr_job()

