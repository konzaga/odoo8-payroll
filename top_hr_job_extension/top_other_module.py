# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2019 top1 africa
# Author: top1
#
# 
# ##############################################################################  -->
from openerp.osv import fields, osv
import datetime
import time

class hr_job_tranche(osv.osv):
    _name = 'hr.job.tranche'
    _rec_name="tranche"
    _description = "tranche categorielle"
   
 
    _columns={
		'tranche': fields.char("tranche",size=50,required=False),
              }
    
 
 
hr_job_tranche()


class hr_job_family(osv.osv):
    _name = 'hr.job.family'
    _rec_name="famille"
    _description = "famille employe"
    
 
    _columns={
		'famille': fields.char("Famille",size=50,required=False),
              }
    
    
hr_job_family()


class hr_job_sfamily(osv.osv):
    _name = 'hr.job.sfamily'
    _rec_name="sfamille"
    _description = "sous famille employe"
    
 
    _columns={
		'sfamille': fields.char("Sous Famille",size=50,required=False),
              }
    
    
hr_job_sfamily()

class hr_job_direction(osv.osv):
    _name = 'hr.job.direction'
    _rec_name="direction"
    _description = "job direction"
    
 
    _columns={
		'direction': fields.char("Direction",size=50,required=False),
              }
    
    
hr_job_direction()



class hr_job_service(osv.osv):
    _name = 'hr.job.service'
    _rec_name="service"
    _description = "Job service"
    
 
    _columns={
		'service': fields.char("Service",size=50,required=False),
              }
    
    
hr_job_service()


class hr_job_employee_mission (osv.osv):
    _name = 'hr.job.employe.mission'
    _description = "Job mission"
    
 
    _columns={
        'sequence': fields.integer(string="Séquence",required=False),
		'mission': fields.text(string="Mission",size=300,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_employee_mission()


class hr_job_employee_mission2 (osv.osv):
    _name = 'hr.job.employe.mission2'
    _description = "Job mission"
    
 
    _columns={
        'sequence': fields.integer(string="Séquence",required=False),
		'mission': fields.text(string="Mission",size=300,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_employee_mission2()


class hr_job_employee_tasks (osv.osv):
    _name = 'hr.job.employe.tasks'
    _description = "Job tasks"
    
 
    _columns={
        'tsequence': fields.integer("Séquence",required=False),
		'tache': fields.char("Tache",size=300,required=False),
        'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_employee_tasks()


class hr_job_employee_tasks2 (osv.osv):
    _name = 'hr.job.employe.tasks2'
    _description = "Job tasks"
    
 
    _columns={
        'tsequence': fields.integer("Séquence",required=False),
		'tache': fields.text("Tache",size=300,required=False),
        'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_employee_tasks2()


class hr_job_employee_tasks3 (osv.osv):
    _name = 'hr.job.employe.tasks3'
    _description = "Job tasks"
    
 
    _columns={
        'tsequence': fields.integer("Séquence",required=False),
		'tache': fields.text("Tache",size=300,required=False),
        'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_employee_tasks3()

class hr_job_kpi (osv.osv):
    _name = 'hr.job.kpi'
    _description = "Job kpi"
    
 
    _columns={
        'ksequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=300,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_kpi()

class hr_job_school (osv.osv):
    _name = 'hr.job.school'
    _description = "Job school"
    
 
    _columns={
        'nom': fields.char("Nom",required=False),
		'niveau': fields.char("Niveau",size=200,required=False),
		'domaine': fields.char("Domaine",size=200,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_school()


class hr_job_experience (osv.osv):
    _name = 'hr.job.experience'
    _description = "Job experience"
    
 
    _columns={
        'esequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_experience()

class hr_job_competence (osv.osv):
    _name = 'hr.job.competence'
    _description = "Job comptétence"
    
 
    _columns={
        'csequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_competence()


class hr_job_competence1 (osv.osv):
    _name = 'hr.job.competence1'
    _description = "Job comptétence"
    
 
    _columns={
        'csequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_competence1()

class hr_job_competence2 (osv.osv):
    _name = 'hr.job.competence2'
    _description = "Job comptétence"
    
 
    _columns={
        'csequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_competence2()


class hr_job_competence3 (osv.osv):
    _name = 'hr.job.competence3'
    _description = "Job comptétence"
    
 
    _columns={
        'csequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
    
hr_job_competence3()

class hr_job_generale_condition (osv.osv):
    _name = 'hr.job.generale.condition'
    _description = "Job generale condition"
    
 
    _columns={
        'gsequence': fields.integer("Séquence",required=False),
		'Nom': fields.text("Nom",size=500,required=False),
		'job_id': fields.many2one("hr.job","job"),
              }
    
hr_job_generale_condition()











