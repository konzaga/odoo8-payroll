##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_emprunt
# ##############################################################################
{
    "name" : "top_hr_job_extension",
    "version" : "3.2",
    "author" : "Diaby Drissa",
    "website" : "www.top1africa-informatique.com",
    "depends" : ["hr","hr_contract"],
    "description": """ """,
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : ["top_hr_job_extension_view.xml",
					"top_other_module_view.xml",],
    "installable": True
}
