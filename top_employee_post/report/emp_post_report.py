# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd. (<http://devintellecs.com>).
#
##############################################################################
#import time
from datetime import datetime
#from dateutil import relativedelta
from openerp.osv import fields, osv
from openerp.report import report_sxw
from openerp.tools import amount_to_text_en


class emp_post_report(report_sxw.rml_parse): 
    def __init__(self, cr, uid, name, context):
        super(emp_post_report, self).__init__(cr, uid, name, context)
        self.localcontext.update({

         
        })
          
    
class dev_emp_profile_report_print(osv.AbstractModel):
    _name = 'report.top_employee_post.emp_detail_report' 
    _inherit = 'report.abstract_report'
    _template = 'top_employee_post.emp_detail_report'
    _wrapped_report_class = emp_post_report 

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


