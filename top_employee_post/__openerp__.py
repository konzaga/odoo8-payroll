# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    
#
##############################################################################

{
    'name' : 'Employee Post Report',
	'version' : '2.0',
	'category': 'Generic Modules/Human Resources',
    'sequence':1,
    'summary': 'Employee profile report',
	'description' : """Prints Reports about employee's post""",
	'author' : 'Diaby Drissa',
	'images': [],
	'depends' : ['report','hr'],
	'data' : [
				'report/emplo_post_report_template.xml',
				'report/emp_post_report_call.xml',
		],	
	'demo' : [],
	'test' : [],
    'qweb' : [],
    'installable' : True,
	'auto_install' : False,	
	'application' : True,
    	
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:



	
