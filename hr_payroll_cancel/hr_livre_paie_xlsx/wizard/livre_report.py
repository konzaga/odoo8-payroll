# -*- coding: utf-8 -*-
from openpyxl.styles.borders import Border, Side 
from openerp.osv import  osv 
from openerp import models, fields, api, _
from openerp.addons.report_xlsx.report.report_xlsx import ReportXlsx
from datetime import datetime
from lxml import etree
import unicodedata
from cStringIO import StringIO
from openerp.exceptions import except_orm, Warning, RedirectWarning
import openpyxl, platform, os.path, xlwt, time, calendar, base64, logging, openerp.addons.decimal_precision as dp
from openpyxl import load_workbook
from openpyxl import workbook
import xlwt
from openerp.modules.module import *


class WizardExcelReport(models.TransientModel):
    _name = "wizard.excel.report"
    
    report = fields.Binary('Prepared file',filters='.xls', readonly=True)
    name = fields.Char('File Name', size=32)




class hr_payroll(models.TransientModel):
    _name="hr.payroll"
    _inherit="hr.payroll"

    @api.multi
    def genarate_excel_report(self): # fonction report tva

        #module_path= r'/odoo/custom/addons/l10n_fisctax_civ/templates/tva_template.xlsx'
        
        self.compute_hr_payroll()
        line = self.env['hr.payroll.line']
        lien = line.search([('hr_payroll_id','=',self.id)])
        wbk= xlwt.Workbook()
        #wks = wbk.active

       
        style0 = xlwt.easyxf('font: name Times New Roman bold on;align: horiz left;', num_format_str='#,##0.00')

        sheet = wbk.add_sheet("Livre de paie")
        sheet.write(0,0,'MATRICULE', style0)
        sheet.write(0,1,'NOM', style0)
        sheet.write(0,2,'FONCTION', style0)
        sheet.write(0,3,'PAYROLL', style0)
        sheet.write(0,4,'SERVICE', style0)
        sheet.write(0,5,'DEPARTEMENT', style0)
        sheet.write(0,6,'LOCAL', style0)
        sheet.write(0,7,'Salaire de base', style0)
        sheet.write(0,8,'Sursalaire', style0)
        sheet.write(0,9,'Jour', style0)
        sheet.write(0,10,'Allocation conge expatrie', style0)
        sheet.write(0,11,'Preavis', style0)
        sheet.write(0,12,'Allocation conge', style0)
        sheet.write(0,13,'Rappel sur salaire', style0)
        sheet.write(0,14,'Participation frais funeraires', style0)
        sheet.write(0,15,'Trop percu a deduire', style0)
        sheet.write(0,16,'Rappel sur augmentation', style0)
        sheet.write(0,17,'Indemnite depart', style0)
        sheet.write(0,18,'Indemnite fin CDD', style0)
        sheet.write(0,19,'Indemnite licenciement', style0)
        sheet.write(0,20,'Indemnite de Logement', style0)
        sheet.write(0,21,'Prime d anciennete', style0)
        sheet.write(0,22,'Prime de responsabilite', style0)
        sheet.write(0,23,'Gratifications', style0)
        sheet.write(0,24,'Prime de caisse', style0)
        sheet.write(0,25,'Prime exceptionnel', style0)
        sheet.write(0,26,'Prime d intervention', style0)
        sheet.write(0,27,'Dotation en Carburant', style0)
        sheet.write(0,28,'Avantages en Nature', style0)
        sheet.write(0,29,'Prime de transport imposable', style0)
        sheet.write(0,30,'HEURE SUPPL 15 pour cent', style0)
        sheet.write(0,31,'HEURE SUPPL 50 pour cent', style0)
        sheet.write(0,32,'HEURE SUPPL 75 pour cent', style0)
        sheet.write(0,33,'HEURE SUPPL 100 pour cent', style0)
        sheet.write(0,34,'Salaire Brut', style0)
        sheet.write(0,35,'Base Imposable CNPS', style0)
        sheet.write(0,36,'Abatement 10 pour cent', style0)
        sheet.write(0,37,'Base imposable impot', style0)
        sheet.write(0,38,'Contribution Nationale (CN)', style0)
        sheet.write(0,39,'Impot sur trait et sal (IS)', style0)
        sheet.write(0,40,'Impot sur trait et sal (ITS)', style0)
        sheet.write(0,41,'Impot General sur Revenu (IGR)', style0)
        sheet.write(0,42,'Retraite Generale (C N P S)', style0)
        sheet.write(0,43,'Retraite Generale (C N P S)', style0)
        sheet.write(0,44,'Prestations Familiales', style0)
        sheet.write(0,45,'Accidents du Travail', style0)
        sheet.write(0,46,'Taxe d apprentissage', style0)
        sheet.write(0,47,'Total Retenues', style0)
        sheet.write(0,48,'Reprise avantage en nature', style0)
        sheet.write(0,49,'Prime outillage', style0)
        sheet.write(0,50,'Prime de transport', style0)
        sheet.write(0,51,'Prime de salissure', style0)
        sheet.write(0,52,'Autres Primes', style0)
        sheet.write(0,53,'Prime de direction', style0)
        sheet.write(0,54,'Prime de presence', style0)
        sheet.write(0,55,'Prime de non accident', style0)
        sheet.write(0,56,'Prime de deplacement', style0)
        sheet.write(0,57,'Prime de depannage', style0)
        sheet.write(0,58,'Prime de bonne conduite', style0)
        sheet.write(0,59,'Autre Indemnite non imposable', style0)
        sheet.write(0,60,'Salaire Brut Total', style0)
        sheet.write(0,61,'Total des charges salariales', style0)
        sheet.write(0,62,'Assurance maladie', style0)
        sheet.write(0,63,'Pret logement', style0)
        sheet.write(0,64,'Mutuelle Groupe MEDLOG', style0)
        sheet.write(0,65,'Pret scolaire', style0)
        sheet.write(0,66,'Retenues pour absences', style0)
        sheet.write(0,67,'Net Imposable', style0)
        sheet.write(0,68,'Arrondi de paie', style0)
        sheet.write(0,69,'Avance sur salaire', style0)
        sheet.write(0,70,'Net', style0)
        sheet.write(0,71,'Prime de panier', style0)
        sheet.write(0,72,'Prime de rendement', style0)
        sheet.write(0,73,'Indemnite deces', style0)
        sheet.write(0,74,'Indemnite licenciement non imposable', style0)
        sheet.write(0,75,'Indemnite de representation', style0)
        sheet.write(0,76,'Taxe Formation Prof Continue', style0)
        sheet.write(0,77,'Total Charges Patronales', style0)
        sheet.write(0,78,'Retenues achat interne', style0)
        sheet.write(0,79,'Confection badge egare', style0)
        sheet.write(0,80,'Prel cantine', style0)
        sheet.write(0,81,'Frais remboursables', style0)
        sheet.write(0,82,'Pret MATSP', style0)
        sheet.write(0,83,'Pret exceptionnel', style0)
        
        

        row = 1
        for rec in lien:                
            sheet.write(row,0,rec.matricule, style0)
            sheet.write(row,1,rec.name, style0)
            sheet.write(row,2,rec.poste, style0)
            sheet.write(row,3,rec.office_id, style0)
            sheet.write(row,4,rec.service, style0)
            sheet.write(row,5,rec.department, style0)
            sheet.write(row,6,rec.location, style0)
            sheet.write(row,7,rec.salaire_base, style0)
            sheet.write(row,8,rec.sursalaire, style0)
            sheet.write(row,9,rec.jour, style0)
            sheet.write(row,10,rec.allocation, style0)
            sheet.write(row,11,rec.preavis, style0)
            sheet.write(row,12,rec.alloconge, style0)
            sheet.write(row,13,rec.rapels, style0)
            sheet.write(row,14,rec.participation, style0)
            sheet.write(row,15,rec.percu, style0)
            sheet.write(row,16,rec.rapelaugment, style0)
            sheet.write(row,17,rec.indemite, style0)
            sheet.write(row,18,rec.indemtecdd, style0)
            sheet.write(row,19,rec.indemlicen, style0)
            sheet.write(row,20,rec.indemloge, style0)
            sheet.write(row,21,rec.primeancien, style0)
            sheet.write(row,22,rec.primeresp, style0)
            sheet.write(row,23,rec.gratifi, style0)
            sheet.write(row,24,rec.primecais, style0)
            sheet.write(row,25,rec.primeexcep, style0)
            sheet.write(row,26,rec.primeinter, style0)
            sheet.write(row,27,rec.dotation, style0)
            sheet.write(row,28,rec.avantage_nature, style0)
            sheet.write(row,29,rec.primetrans, style0)
            sheet.write(row,30,rec.hs15, style0)
            sheet.write(row,31,rec.hs50, style0)
            sheet.write(row,32,rec.hs75, style0)
            sheet.write(row,33,rec.hs100, style0)
            sheet.write(row,34,rec.brut, style0)
            sheet.write(row,35,rec.base_imp, style0)
            sheet.write(row,36,rec.abatement, style0)
            sheet.write(row,37,rec.baseimpo, style0)
            sheet.write(row,38,rec.cn, style0)
            sheet.write(row,39,rec.IS, style0)
            sheet.write(row,40,rec.its, style0)
            sheet.write(row,41,rec.igr, style0)
            sheet.write(row,42,rec.cnps, style0)
            sheet.write(row,43,rec.cnpsp, style0)
            sheet.write(row,44,rec.Prestafamille, style0)
            sheet.write(row,45,rec.Accident_trav, style0)
            sheet.write(row,46,rec.taxe_app, style0)
            sheet.write(row,47,rec.retenu_total, style0)
            sheet.write(row,48,rec.reprise, style0)
            sheet.write(row,49,rec.primeoutil, style0)
            sheet.write(row,50,rec.primetran2, style0)
            sheet.write(row,51,rec.PSALI, style0)
            sheet.write(row,52,rec.AUTRES, style0)
            sheet.write(row,53,rec.PDIR, style0)
            sheet.write(row,54,rec.PPRES, style0)
            sheet.write(row,55,rec.PNACC, style0)
            sheet.write(row,56,rec.PDPL, style0)
            sheet.write(row,57,rec.PDPN, style0)
            sheet.write(row,58,rec.PBCO, style0)
            sheet.write(row,59,rec.INDM_NON_IMP, style0)
            sheet.write(row,60,rec.BRUT_TOTAL, style0)
            sheet.write(row,61,rec.SALC, style0)
            sheet.write(row,62,rec.ASSMAL, style0)
            sheet.write(row,63,rec.PLOG, style0)
            sheet.write(row,64,rec.MUT, style0)
            sheet.write(row,65,rec.SCOL, style0)
            sheet.write(row,66,rec.EMP, style0)
            sheet.write(row,67,rec.C_IMP, style0)
            sheet.write(row,68,rec.ARR, style0)
            sheet.write(row,69,rec.SAR, style0)
            sheet.write(row,70,rec.NET, style0)
            sheet.write(row,71,rec.PPAN, style0)
            sheet.write(row,72,rec.PREND, style0)
            sheet.write(row,73,rec.IDEC, style0)
            sheet.write(row,74,rec.ILICEN_NI, style0)
            sheet.write(row,75,rec.IREPR, style0)
            sheet.write(row,76,rec.TAXEFP, style0)
            sheet.write(row,77,rec.TCOMP, style0)
            sheet.write(row,78,rec.ACHINT, style0)
            sheet.write(row,79,rec.BADG, style0)
            sheet.write(row,80,rec.CANT, style0)
            sheet.write(row,81,rec.FRAIS, style0)
            sheet.write(row,82,rec.PMATSP, style0)
            sheet.write(row,83,rec.PEXCP, style0)
          
            row +=1

       

        wbk.save(r'/tmp/livre_template.xls')
        result_file = open(r'/tmp/livre_template.xls','rb').read()
        attach_id = self.env['wizard.excel.report'].create({
                                        'name':'livre paie.xls',
                                        'report':base64.encodestring(result_file)
                    })

        return {
            'name': _('Notification'),
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.excel.report',
            'res_id':attach_id.id,
            'data': None,
            'type': 'ir.actions.act_window',
            'target':'new'
        }