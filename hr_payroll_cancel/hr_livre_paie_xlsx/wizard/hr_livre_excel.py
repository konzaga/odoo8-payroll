# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################
import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
from openerp.osv import osv, fields, orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.osv import osv,fields
from openerp import api,modules

class hr_payroll(osv.osv):
    _name="hr.payroll"
    _description="Livre de paie"
    _columns={
            "name":fields.char("Libellé",size=28),
            "date_from":fields.date('Date de début',required=True),
            "date_to":fields.date('Date de fin',required=True),
            "line_ids":fields.one2many('hr.payroll.line','hr_payroll_id',"Lignes de livre de paie"),
            "company_id":fields.many2one('res.company','Compagnie'),
            'partner_id':fields.many2one('res.partner','Parténaire'),
        }
    
    _defaults = {  
        'date_from': lambda *a: time.strftime('%Y-%m-01'),  
        'date_to': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
        "company_id":1,
        'partner_id':1,
        }
    
    
    def get_amount_by_code(self,cr,uid,line_ids,code,context=None):
        for line in line_ids:
            if line.code==code :
                return line.total
            
    def get_amount_by_category(self,cr,uid,line_ids,category,context=None):
        rcategory_obj=self.pool.get('hr.salary.rule.category')
        category_id=rcategory_obj.search(cr,uid,[('code',"=",category)])
        montant=0        
        if category_id :
            for line in line_ids :
                if line.category_id == category_id[0] :
                    montant+=line.total
        return montant
    
    def compute_hr_payroll(self,cr,uid,ids,context=None):
        hpslip_obj=self.pool.get('hr.payslip')
        hpline_obj=self.pool.get('hr.payroll.line')
        for i in self.browse(cr, uid, ids, context):
            if i.line_ids :
                cr.execute("DELETE FROM hr_payroll_line WHERE hr_payroll_id=%s"%i.id)
                cr.commit()
            res={'value':{'line_ids':False}}
            slip_ids=hpslip_obj.search(cr,uid,[('date_from','<=',i.date_from),('date_to','>=',i.date_to)])
            lines={}
            for slip in hpslip_obj.browse(cr,uid,slip_ids,context):
                vals={
                        "matricule":slip.employee_id.identification_id,
                        "name":slip.employee_id.name,
                        "poste":slip.employee_id.job_id.name,
                        "office_id":slip.employee_id.office_id.office,
                        "service":slip.employee_id.service.service,
                        "department":slip.employee_id.department_id.name,
                        "location":slip.employee_id.location.bureau,
                        'PPAN':self.get_amount_by_code(cr, uid, slip.line_ids, "PPAN", context),
                        'PREND':self.get_amount_by_code(cr, uid, slip.line_ids, "PREND", context),
                        'IDEC':self.get_amount_by_code(cr, uid, slip.line_ids, "IDEC", context),
                        'ILICEN_NI':self.get_amount_by_code(cr, uid, slip.line_ids, "ILICEN_NI", context),
                        'IREPR':self.get_amount_by_code(cr, uid, slip.line_ids, "IREPR", context),
                        'TAXEFP':self.get_amount_by_code(cr, uid, slip.line_ids, "TAXEFP", context),
                        'TCOMP':self.get_amount_by_code(cr, uid, slip.line_ids, "TCOMP", context),
                        'ACHINT':self.get_amount_by_code(cr, uid, slip.line_ids, "ACHINT", context),
                        'BADG':self.get_amount_by_code(cr, uid, slip.line_ids, "BADG", context),
                        'CANT':self.get_amount_by_code(cr, uid, slip.line_ids, "CANT", context),
                        'FRAIS':self.get_amount_by_code(cr, uid, slip.line_ids, "FRAIS", context),
                        'PMATSP':self.get_amount_by_code(cr, uid, slip.line_ids, "PMATSP", context),
                        'PEXCP':self.get_amount_by_code(cr, uid, slip.line_ids, "PEXCP", context),
                        'salaire_base':self.get_amount_by_code(cr, uid, slip.line_ids, "BASE", context),
                        "sursalaire":self.get_amount_by_code(cr, uid, slip.line_ids, "SURSA", context),
                        "jour":self.get_amount_by_code(cr, uid, slip.line_ids, "JR", context),
                        "allocation":self.get_amount_by_code(cr, uid, slip.line_ids, "ALLEXP", context),
                        "preavis":self.get_amount_by_code(cr, uid, slip.line_ids, "PREAV", context),
                        "alloconge":self.get_amount_by_code(cr, uid, slip.line_ids, "ALL", context),
                        "rapels":self.get_amount_by_code(cr, uid, slip.line_ids, "RSAL", context),
                        "participation":self.get_amount_by_code(cr, uid, slip.line_ids, "PARTFF", context),
                        "percu":self.get_amount_by_code(cr, uid, slip.line_ids, "TPERD", context),
                        "rapelaugment":self.get_amount_by_code(cr, uid, slip.line_ids, "RAPAUG", context),
                        "indemite":self.get_amount_by_code(cr, uid, slip.line_ids, "IDEP", context),
                        "indemtecdd":self.get_amount_by_code(cr, uid, slip.line_ids, "ICDD", context),
                        "indemlicen":self.get_amount_by_code(cr, uid, slip.line_ids, "ILICEN", context),
                        "indemloge":self.get_amount_by_code(cr, uid, slip.line_ids, "INDML", context),
                        "primeancien":self.get_amount_by_code(cr, uid, slip.line_ids, "PANC", context),
                        "primeresp":self.get_amount_by_code(cr, uid, slip.line_ids, "PRES", context),
                        "gratifi":self.get_amount_by_code(cr, uid, slip.line_ids, "GRATIF", context),
                        "primecais":self.get_amount_by_code(cr, uid, slip.line_ids, "PCAIS", context),
                        "primeexcep":self.get_amount_by_code(cr, uid, slip.line_ids, "EXCEP", context),
                        "primeinter":self.get_amount_by_code(cr, uid, slip.line_ids, "INTERV", context),
                        "dotation":self.get_amount_by_code(cr, uid, slip.line_ids, "CARBU", context),
                        "avantage_nature":self.get_amount_by_code(cr, uid, slip.line_ids, "AVTGN", context),
                        "primetrans":self.get_amount_by_code(cr, uid, slip.line_ids, "TRSP_IMP", context),
                        "hs15":self.get_amount_by_code(cr, uid, slip.line_ids, "HS15", context),
                        "hs50":self.get_amount_by_code(cr, uid, slip.line_ids, "HS50", context),
                        "hs75":self.get_amount_by_code(cr, uid, slip.line_ids, "HS75", context),
                        "hs100":self.get_amount_by_code(cr, uid, slip.line_ids, "HS100", context),
                        "brut":self.get_amount_by_code(cr, uid, slip.line_ids, "BRUT", context),
                        "base_imp":self.get_amount_by_code(cr, uid, slip.line_ids, "BASE_CNPS", context),
                        "abatement":self.get_amount_by_code(cr, uid, slip.line_ids, "ABAT_10", context),
                        "baseimpo":self.get_amount_by_code(cr, uid, slip.line_ids, "BASE_IMP", context),
                        "cn":self.get_amount_by_code(cr, uid, slip.line_ids, "CN", context),
                        "IS":self.get_amount_by_code(cr, uid, slip.line_ids, "ITS", context),
                        "its":self.get_amount_by_code(cr, uid, slip.line_ids, "ITS_P", context),
                        "igr":self.get_amount_by_code(cr, uid, slip.line_ids, "IGR", context),
                        "cnps":self.get_amount_by_code(cr, uid, slip.line_ids, "CNPS", context),
                        "cnpsp":self.get_amount_by_code(cr, uid, slip.line_ids, "CNPS_P", context),
                        "Prestafamille":self.get_amount_by_code(cr, uid, slip.line_ids, "PF", context),
                        "Accident_trav":self.get_amount_by_code(cr, uid, slip.line_ids, "ACT", context),
                        "taxe_app":self.get_amount_by_code(cr, uid, slip.line_ids, "TAXEAP", context),
                        "retenu_total":self.get_amount_by_code(cr, uid, slip.line_ids, "RET", context),
                        "reprise":self.get_amount_by_code(cr, uid, slip.line_ids, "RAVTGN", context),
                        "primeoutil":self.get_amount_by_code(cr, uid, slip.line_ids, "POUTIL", context),
                        "primetran2":self.get_amount_by_code(cr, uid, slip.line_ids, "TRSP", context),
                        "PSALI":self.get_amount_by_code(cr, uid, slip.line_ids, "PSALI", context),
                        "AUTRES":self.get_amount_by_code(cr, uid, slip.line_ids, "AUTRES", context),
                        "PDIR":self.get_amount_by_code(cr, uid, slip.line_ids, "PDIR", context),
                        "PPRES":self.get_amount_by_code(cr, uid, slip.line_ids, "PPRES", context),
                        "PNACC":self.get_amount_by_code(cr, uid, slip.line_ids, "PNACC", context),
                        "PDPL":self.get_amount_by_code(cr, uid, slip.line_ids, "PDPL", context),
                        "PDPN":self.get_amount_by_code(cr, uid, slip.line_ids, "PDPN", context),
                        "PBCO":self.get_amount_by_code(cr, uid, slip.line_ids, "PBCO", context),
                        "INDM_NON_IMP":self.get_amount_by_code(cr, uid, slip.line_ids, "INDM_NON_IMP", context),
                        "BRUT_TOTAL":self.get_amount_by_code(cr, uid, slip.line_ids, "BRUT_TOTAL", context),
                        "SALC":self.get_amount_by_code(cr, uid, slip.line_ids, "SALC", context),
                        "ASSMAL":self.get_amount_by_code(cr, uid, slip.line_ids, "ASSMAL", context),
                        "PLOG":self.get_amount_by_code(cr, uid, slip.line_ids, "PLOG", context),
                        "MUT":self.get_amount_by_code(cr, uid, slip.line_ids, "MUT", context),
                        "SCOL":self.get_amount_by_code(cr, uid, slip.line_ids, "SCOL", context),
                        "EMP":self.get_amount_by_code(cr, uid, slip.line_ids, "EMP", context),
                        "C_IMP":self.get_amount_by_code(cr, uid, slip.line_ids, "C_IMP", context),
                        "ARR":self.get_amount_by_code(cr, uid, slip.line_ids, "ARR", context),
                        "SAR":self.get_amount_by_code(cr, uid, slip.line_ids, "SAR", context),
                        "NET":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),

        
                        "hr_payroll_id":i.id,
                        }
                line_id=hpline_obj.create(cr,uid,vals,context)
                
hr_payroll()



class hr_payroll_line(osv.osv):
    _name="hr.payroll.line"
    _description="Ligne de livre de paie"
    _columns = {
                "matricule":fields.char("Matricule",size=128,required=False),
                "name":fields.char("Nom et Prenoms",size=128,required=False),
                "poste":fields.char("Poste",size=128,required=False),
                "office_id":fields.char("Payroll",size=128,required=False),
                "service":fields.char("Service",size=128,required=False),
                "department":fields.char("Departement",size=128,required=False),
                "location":fields.char("Localisation",size=128,required=False),
                "salaire_base":fields.integer("Base",requred=False),
                "sursalaire":fields.integer("Sursa",required=False),
                "jour":fields.integer("Jour",required=False),
                "allocation":fields.integer("Allocation conge expatrie",required=False),
                "preavis":fields.integer("Preavis",required=False),
                "alloconge":fields.integer("Allocation conge",required=False),
                "rapels":fields.integer("Rappel sur salaire",required=False),
                "participation":fields.integer("Participation frais funeraires",required=False),
                "percu":fields.integer("Trop perçu a deduire",required=False),
                "rapelaugment":fields.integer("Rappel sur augmentation",required=False),
                "indemite":fields.integer("Indemnite depart",required=False),
                "indemtecdd":fields.integer("Indemnite fin CDD",required=False),
                "indemlicen":fields.integer("Indemnite licenciement",required=False),
                "indemloge":fields.integer("Indemnite de Logement",required=False),
                "primeancien":fields.integer("Prime d anciennete",required=False),
                "primeresp":fields.integer("Prime de responsabilite",required=False),
                "gratifi":fields.integer("Gratifications",required=False),
                "primecais":fields.integer("Prime de caisse",required=False),
                "primeexcep":fields.integer("Prime exceptionnel",required=False),
                "primeinter":fields.integer("Prime d'intervention",required=False),
                "dotation":fields.integer("Dotation en Carburant",required=False),
                "avantage_nature":fields.integer("Avantages en Nature",required=False),
                "primetrans":fields.integer("Prime de transport imposable",required=False),
                "hs15":fields.integer("HEURE SUPPL 15",required=False),
                "hs50":fields.integer("HEURE SUPPL 50",required=False),
                "hs75":fields.integer("HEURE SUPPL 75",required=False),
                "hs100":fields.integer("HEURE SUPPL 100",required=False),
                "brut":fields.integer("Salaire Brut",required=False),
                "base_imp":fields.integer("Base Imposable CNPS",required=False),
                "abatement":fields.integer("Abatement 10",required=False),
                "baseimpo":fields.integer("Base imposable impot",required=False),
                "cn":fields.integer("Contribution Nationale (CN)",required=False),
                "IS":fields.integer("Impot sur trait et sal (IS)",required=False),
                "its":fields.integer("Impot sur trait et sal (ITS)",required=False),
                "igr":fields.integer("Impot General sur Revenu (IGR)",required=False),
                "cnps":fields.integer("Retraite Generale (C N P S)",required=False),
                "cnpsp":fields.integer("Retraite Generale (C N P S) P",required=False),
                "Prestafamille":fields.integer("Prestations Familiales",required=False),
                "Accident_trav":fields.integer("Accidents du Travail",required=False),
                "taxe_app":fields.integer("Taxe d apprentissage",required=False),
                "retenu_total":fields.integer("Total Retenues",required=False),
                "reprise":fields.integer("Reprise avantage en nature",required=False),
                "primeoutil":fields.integer("Prime outillage",required=False),
                "primetran2":fields.integer("Prime de transport",required=False),
                "PSALI":fields.integer("Prime de salissure",required=False),
                "AUTRES":fields.integer("Autres Primes",required=False),
                "PDIR":fields.integer("Prime de direction",required=False),
                "PPRES":fields.integer("Prime de presence",required=False),
                "PNACC":fields.integer("Prime de non accident",required=False),
                "PDPL":fields.integer("Prime de deplacement",required=False),
                "PDPN":fields.integer("Prime de depannage",required=False),
                "PBCO":fields.integer("Prime de bonne conduite",required=False),
                "INDM_NON_IMP":fields.integer("Autre Indemnité non imposable",required=False),
                "BRUT_TOTAL":fields.integer("Salaire Brut Total",required=False),
                "SALC":fields.integer("Total des charges salariales",required=False),
                "ASSMAL":fields.integer("Assurance maladie",required=False),
                "PLOG":fields.integer("Prêt logement",required=False),
                "MUT":fields.integer("Mutuelle Groupe MEDLOG",required=False),
                "SCOL":fields.integer("Prêt scolaire",required=False),
                "EMP":fields.integer("Retenues pour absences",required=False),
                "C_IMP":fields.integer("Net Imposable",required=False),
                "ARR":fields.integer("Arrondi de paie",required=False),
                "SAR":fields.integer("Avance sur salaire",required=False),
                "NET":fields.integer("Net",required=False),
                'PPAN':fields.integer("PPAN",required=False),
                'PREND':fields.integer("PREND",required=False),
                'IDEC':fields.integer("IDEC",required=False),
                'ILICEN_NI':fields.integer("ILICEN_NI",required=False),
                'IREPR':fields.integer("IREPR",required=False),
                'TAXEFP':fields.integer("TAXEFP",required=False),
                'TCOMP':fields.integer("TCOMP",required=False),
                'ACHINT':fields.integer("ACHINT",required=False),
                'BADG':fields.integer("BADG",required=False),
                'CANT':fields.integer("CANT",required=False),
                'FRAIS':fields.integer("FRAIS",required=False),
                'PMATSP':fields.integer("PMATSP",required=False),
                'PEXCP':fields.integer("PEXCP",required=False),

                "hr_payroll_id":fields.many2one('hr.payroll',"Livre de paie",required=True),
            }
hr_payroll_line()
