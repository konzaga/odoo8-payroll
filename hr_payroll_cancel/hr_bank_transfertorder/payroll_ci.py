# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2014 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_payroll_ci_raport
# ##############################################################################


import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta


from openerp.osv import osv,fields
from openerp import api,modules

class res_partner_bank(osv.osv):
     _inherit = 'res.partner.bank'


     _columns={
             "guichet":fields.char("Code guichet",size=20,required=True),

     }





class hr_bank(osv.osv):
    _name="hr.bank.transfert"
    _description="Transfert Bancaire"
    _columns={
            "name":fields.char("Libellé",size=28),
            "date_from":fields.date('Date de début',required=True),
            "date_to":fields.date('Date de fin',required=True),
            "line_ids":fields.one2many('hr.bank.transfert.line','hr_transfert_id',"Lignes de transfert"),
            "payment_method":fields.selection([('espece','Espèces'),('virement','Virement bancaire'),('cheque','Chèques')],
                                                string='Moyens de paiement',required=True),
            "bank":fields.many2one("res.bank","Banque",required=True),
            "bureau":fields.many2one("hr.bureau", 'Site', required=False),
            "col":fields.many2one("hr.employee.category", 'Couleur de col', required=False),
        }
    
    _defaults = {  
        'date_from': lambda *a: time.strftime('%Y-%m-01'),  
        'date_to': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
        
        }
    
    def print_payroll(self, cr, uid, ids, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        
        self.compute_hr_payroll(cr, uid, ids, context)
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'hr_bank_transfertorder.report_bank', context=context)
    
    
    def get_amount_by_code(self,cr,uid,line_ids,code,context=None):
        for line in line_ids:
            if line.code==code :
                return line.total
        
    
    def compute_hr_payroll(self,cr,uid,ids,context=None):
        hpslip_obj=self.pool.get('hr.payslip')
        hpline_obj=self.pool.get('hr.bank.transfert.line')
        for i in self.browse(cr, uid, ids, context):
            if i.line_ids :
                cr.execute("DELETE FROM hr_bank_transfert_line WHERE hr_transfert_id=%s"%i.id)
                cr.commit()
            res={'value':{'line_ids':False}}
            slip_ids=hpslip_obj.search(cr,uid,[('date_from','<=',i.date_from),('date_to','>=',i.date_to),('payment_method','=',i.payment_method)])
            lines={}
            for slip in hpslip_obj.browse(cr,uid,slip_ids,context):
                if i.bureau and not i.col :
                    if slip.employee_id.location == i.bureau and slip.employee_id.bank_account_id.bank == i.bank :
                        vals={
                                'matricule':slip.employee_id.identification_id,
                                'beneficiare':slip.identification_id,
                                "domicile":slip.employee_id.bank_account_id.bank.name,
                                "code_bank":slip.employee_id.bank_account_id.bank_bic,
                                "guichet":slip.employee_id.bank_account_id.guichet,
                                "num_compte":slip.employee_id.bank_account_id.acc_number,
                                "net":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),
                                "hr_transfert_id":i.id,
                                }
                        line_id=hpline_obj.create(cr,uid,vals,context)

                if i.col and not i.bureau :
                    if slip.employee_id.category_ids == i.col and slip.employee_id.bank_account_id.bank == i.bank :
                        vals={
                                'matricule':slip.employee_id.identification_id,
                                'beneficiare':slip.identification_id,
                                "domicile":slip.employee_id.bank_account_id.bank.name,
                                "code_bank":slip.employee_id.bank_account_id.bank_bic,
                                "guichet":slip.employee_id.bank_account_id.guichet,
                                "num_compte":slip.employee_id.bank_account_id.acc_number,
                                "net":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),
                                "hr_transfert_id":i.id,
                                }
                        line_id=hpline_obj.create(cr,uid,vals,context)

                if not i.bureau and not i.col :
                    if slip.employee_id.bank_account_id.bank == i.bank :
                        vals={
                                'matricule':slip.employee_id.identification_id,
                                'beneficiare':slip.identification_id,
                                "domicile":slip.employee_id.bank_account_id.bank.name,
                                "code_bank":slip.employee_id.bank_account_id.bank_bic,
                                "guichet":slip.employee_id.bank_account_id.guichet,
                                "num_compte":slip.employee_id.bank_account_id.acc_number,
                                "net":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),
                                "hr_transfert_id":i.id,
                                }
                        line_id=hpline_obj.create(cr,uid,vals,context)


                if i.bureau and i.col :
                    if slip.employee_id.bank_account_id.bank == i.bank and slip.employee_id.location == i.bureau and slip.employee_id.category_ids == i.col:
                        vals={
                                'matricule':slip.employee_id.identification_id,
                                'beneficiare':slip.identification_id,
                                "domicile":slip.employee_id.bank_account_id.bank.name,
                                "code_bank":slip.employee_id.bank_account_id.bank_bic,
                                "guichet":slip.employee_id.bank_account_id.guichet,
                                "num_compte":slip.employee_id.bank_account_id.acc_number,
                                "net":self.get_amount_by_code(cr, uid, slip.line_ids, "NET", context),
                                "hr_transfert_id":i.id,
                                }
                        line_id=hpline_obj.create(cr,uid,vals,context)



hr_bank()


class hr_bank_transfert_line(osv.osv):
    _name="hr.bank.transfert.line"
    _description="Ligne de transfert Bancaire"
    _columns = {
                "matricule":fields.char("Matricule",size=128,required=False),
                "beneficiare":fields.char("Beneficiare",size=128,requred=False),
                "domicile":fields.char("Domiciliation",size=128,required=False),
                "code_bank":fields.char("Code banque",size=128,required=False),
                "guichet":fields.char("Code guichet",size=128,required=False),
                "num_compte":fields.char("Numero compte",size=128,required=False),
                "net":fields.integer("Montant"),
                "hr_transfert_id":fields.many2one('hr.bank.transfert',"Livre de paie",required=True),
            }
hr_bank_transfert_line()
