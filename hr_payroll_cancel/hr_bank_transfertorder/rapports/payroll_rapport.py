# -*- coding: utf-8 -*-
# ##############################################################################
# #
# # Copyright (c) 2005-2006 CamptoCamp
# # Copyright (c) 2006-2010 OpenERP S.A
# #
# # WARNING: This program as such is intended to be used by professional
# # programmers who take the whole responsibility of assessing all potential
# # consequences resulting from its eventual inadequacies and bugs
# # End users who are looking for a ready-to-use solution with commercial
# # guarantees and support are strongly advised to contract a Free Software
# # Service Company
# #
# # This program is Free Software; you can redistribute it and/or
# # modify it under the terms of the GNU General Public License
# # as published by the Free Software Foundation; either version 2
# # of the License, or (at your option) any later version.
# #
# # This program is distributed in the hope that it will be useful,
# # but WITHOUT ANY WARRANTY; without even the implied warranty of
# # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# # GNU General Public License for more details.
# #
# # You should have received a copy of the GNU General Public License
# # along with this program; if not, write to the Free Software
# # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
# #
# ##############################################################################
# 
import time
from openerp.osv import osv
from openerp.report import report_sxw



class bank_report(report_sxw.rml_parse):
#     _name = 'report.hr.payroll'

    def __init__(self, cr, uid, name, context):
            super(bank_report, self).__init__(cr, uid, name, context)
            self.localcontext.update({

#                 'get_payslip_lines': self.get_payslip_lines,
            })
            

class report_payroll_raport(osv.AbstractModel):
    _name = 'report.hr_bank_transfertorder.report_bank'
    _inherit = 'report.abstract_report'
    _template = 'hr_bank_transfertorder.report_bank'
    _wrapped_report_class = bank_report
 
 
 
# # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

