{
    "name": "Les Rapports  de Bank",
    "version": "1.0",
    "depends": ["hr","hr_payroll_ci","hr_emprunt","hr_contract_extension"],
    "author": "Diaby Drissa",
    "category": "hr",
    "description": """
    This module provide :
    """,
    "init_xml": [],
    'update_xml': [
                "rapports/payroll_rapport.xml",
                "views/report_payroll.xml",
                "hr_payroll_view.xml",
            ],
    'demo_xml': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}