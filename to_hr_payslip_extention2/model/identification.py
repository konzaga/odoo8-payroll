# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd. (<http://devintellecs.com>).
#
##############################################################################


from openerp import models, fields


class hr_contract(models.Model):
    _inherit = 'hr.contract'

    
    
    identification_id = fields.Char(related='employee_id.name', store=True)
    
	
	
   