# -*- encoding: utf-8 -*-

{
	"name" : "Rapport de paie xls",
   	"version" : "1.0",
   	"author" : "Coulibaly Konzaga Apollinaire",
   	"description" : "Livre de paie avec les noms des employés en colonne et les rubriques en ligne",
   	"category": "Human Resources",
   	"depends" : ["hr_payroll_ci","report_xls"], # liste des module a appeller lor d'un heritage (nom du model parent) ()
   	"data" : [
   		"view/hr_payroll_view.xml",
   		], 
   	"installable": True,
   	"active": False,
}