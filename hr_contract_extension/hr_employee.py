# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_emprunt
# ##############################################################################  -->
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import netsvc



class hr_employee_degree(osv.osv):
    _name = "hr.employee.degree"
    _description = "Degree of employee"
    _columns = {
        'name': fields.char('Name', required=True, translate=True),
        'sequence': fields.integer('Sequence', help="Gives the sequence order when displaying a list of degrees."),
    }
    _defaults = {
        'sequence': 1,
    }
    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the Degree of employee must be unique!')
    ]
hr_employee_degree()


class licence(osv.osv):
    _name = "hr.licence"
    _description = "Licence employé"
    
    _columns={
              'name':fields.char('Libellé Licence', size=64, required=True, readonly=False),
              'reference':fields.char('Reférence', size=64, required=False, readonly=False),
              'date_debut':fields.date('Début validité'),
              'date_fin':fields.date('Fin validité'),
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False),
              }
licence()

class domaine(osv.osv):
    _name = "hr.diplomes.domaine"
    _description = "Domaine de diplome employe"
    _columns={
              'name':fields.char('Libellé Domaine', size=64, required=True, readonly=False),
              }
domaine()  

class diplome_employe(osv.osv):
    _name = "hr.diplomes.employee"
    _description = "Diplome employe"

    _columns={
              'name':fields.char('Name', size=64, required=False, readonly=False,translate=True),
              'diplome_id':fields.many2one('hr.employee.degree', 'Niveau', required=True),
              'domaine_id':fields.many2one('hr.diplomes.domaine', 'Domaines',required=False, readonly=False),
              'reference':fields.char('Reférence', size=64, required=False, readonly=False),
              'date_obtention':fields.date("Date d'obtention"),
              'date_start':fields.date("Date début"),
              'date_end':fields.date("Date fin"),
              'type':fields.selection([('diplome','Diplôme'),('certif','Certification')],"Type",select=True),
              'image':fields.binary('Image'), 
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False)
              }
diplome_employe()


class visa(osv.osv):
    _name = "hr.visa"
    _description = "visa employé"

    _columns={
              'name':fields.char('Libellé visa', size=64, required=True, readonly=False),
              'reference':fields.char('N° Visa', size=64, required=True, readonly=False),
              'pays_id': fields.many2one('res.country', 'Pays', required=True),
              'date_debut':fields.datetime('Début validité'),
              'date_fin':fields.datetime('Fin validité'),
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False)
              }
visa()

class carte_sejour(osv.osv):
    _name = "hr.carte.sejour"
    _description = "Carte de séjour employé"

    _columns={
              'name':fields.char('Libellé visa', size=64, required=False, readonly=False),
              'reference':fields.char('N° Visa', size=64, required=False, readonly=False),
              'pays_id': fields.many2one('res.country', 'Pays', required=False),
              'date_debut':fields.datetime('Début validité'),
              'date_fin':fields.datetime('Fin validité'),
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False)
              }
carte_sejour()


class enfants_employe(osv.osv):
    _name = "hr.employee.enfant"
    _description = "Enfants de l'employé"
    
    _columns={
              'name':fields.char('Nom', size=128, required=True, readonly=False),
              'date_naissance':fields.date("Date de naissance"),
              'mobile':fields.char('Portable', size=128, required=False, readonly=False),
              'email':fields.char('email', size=128, required=False, readonly=False),
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False),
              'lieu':fields.char('Lieu de naissance', size=20),
              'genre':fields.selection([('masculin','Masculin'),('feminin','Feminin')],
                                                string='Genre',required=False),
              }    
enfants_employe()


class hr_parent_employee(osv.osv):
    _name="hr.parent.employe"
    _description = "les parents de l'employee"
    _columns = {
            
              'name':fields.char('Nom', size=128, required=True, readonly=False),
              'date_naissance':fields.date("Date de naissance"),
              'mobile':fields.char('Portable', size=128, required=False, readonly=False),
              'email':fields.char('email', size=128, required=False, readonly=False),
              'employee_id':fields.many2one('hr.employee', 'Employé', required=False),          
        } 

hr_parent_employee()


class personne_contacted(osv.osv):
    _name = 'hr.personne.contacted'
    _description = 'Personnes a contacter'
    _columns = {
        'name':fields.char("Nom",size=128,required=True),
        'email':fields.char("Email",size=128),
        'date_naissance':fields.date("Date de naissance"),
        'mobile':fields.char('Portable',size=128,required=True),
        'link':fields.selection([('conjoint','Conjoint(e)'),
                       ], 'Type de lien', select=True, readonly=False),
        #'Lien':fields.char("Le lien",siez=128),  
        'employee_id':fields.many2one("hr.employee",'Employé'),            
        }  
		
    _defaults={
               'link':'conjoint',
               }

personne_contacted()





class hr_employee(osv.osv):
    def _get_part_igr(self,cr,uid,ids,field,arg,context=None):
        res={}
        for employee in self.browse(cr,uid,ids):
            if employee.marital :
                t1 =employee.marital
                B38 = t1[0]
                B39 = employee.children
                B40 = employee.enfants_a_charge
                
                result = 0
                
                if ((B38 == "s") or (B38 == "d")):
                    if (B39 == 0):
                        if (B40 != 0):
                            result = 1.5
                        else:
                            result = 1
                    else:
                        if ((1.5 +  B39 * 0.5) > 5):
                            result = 5
                        else:
                            result = 1.5 + B39 * 0.5
                else:
                    if (B38 == "m"):
                        if (B39 == 0):
                            result = 2
                        else:
                            if ((2 + 0.5 * B39) > 5):
                                result = 5
                            else:
                                result = 2 + 0.5 * B39
                    else:
                        if (B38 == "w"):
                            if (B39 == 0):
                                if (B40 != 0):
                                    result = 1.5
                                else:
                                    result = 1
                            else:
                                if ((2 + B39 * 0.5) > 5):
                                    result = 5
                                else:
                                    result = 2 + 0.5 * B39
                        else:
                            result += 2 + 0.5 * B39
                res[employee.id] = result
            else:
                res[employee.id] =0
        return res
            
        return res
    _inherit="hr.employee"
    _rec_name ="identification_id"
	
    _columns={
              'matricule_cnps':fields.char('Matricule CNPS',size=64),
              #'matricule_employeur':fields.char('Matricule Employeur', size=64),
              'enfants_a_charge':fields.integer("Nombre d'enfants à charge",required=False),
              'pere':fields.char('Nom du père', size=128, required=False, readonly=False),
              'mere':fields.char('Nom de la mère', size=128, required=False, readonly=False),
              'grade':fields.char('Grade', size=64, required=False, readonly=False),
              'enfants_ids':fields.one2many('hr.employee.enfant', 'employee_id', 'Enfants', required=False),
              'licence_ids': fields.one2many('hr.licence', 'employee_id', 'Licences des employés'),
              'diplome_ids': fields.one2many('hr.diplomes.employee', 'employee_id', 'Diplôme des employés'),
              'visa_ids': fields.one2many('hr.visa', 'employee_id', 'Visas des employés'),
              'carte_sejour_ids': fields.one2many('hr.carte.sejour', 'employee_id', 'Carte de séjour des employés'),
              'part_igr':fields.function(_get_part_igr,method=True,type='float',string='Part IGR'),
              'payment_method':fields.selection([('espece','Espèces'),('virement','Virement bancaire'),('cheque','Chèques')],
                                                string='Moyens de paiement',required=True),
              'date_entree':fields.date("Date d'entrée"),
              'piece_identite_id':fields.many2one("hr.piece.identite","Pièce d'identité"),
              'personnes_contacted_ids':fields.one2many('hr.personne.contacted','employee_id','Conjoint'),
              'parent_employee_ids':fields.one2many("hr.parent.employe",'employee_id','Les parents'),
              'recruitment_degree_id':fields.many2one('hr.employee.degree',"Niveau d'étude"),
              'assurence':fields.integer("Nombres d'assurés",size=10),
              'employe_mar':fields.char('Nom marital',size=24),
	
              }
    _defaults={
               'marital':'single',
               'payment_method':'virement',
               }
		
		
hr_employee()

