# -*- coding: utf-8 -*-


from openerp.osv import osv,fields
from datetime import datetime
from openerp.tools.translate import _


class hr_type_piece(osv.osv):
    _name="hr.type.piece"
    _description="Type de pièce d'identité"
    _columns={
              'name':fields.char("Désignation",size=128,required=True),
              "description":fields.text("Description"),
              }
hr_type_piece()


class hr_piece_identite(osv.osv):
    _name="hr.piece.identite"
    _rec_name="numero_piece"
    _decription="Pièce d'identité"
    _columns={
              "numero_piece":fields.char("Numéro de la pièce",size=128,required=True),
              "nature_piece":fields.selection([('attestion',"Attestation d'indentité"),("carte_sejour","Carte de séjour"),
                                               ("cni","CNI"),("passeport","Passeport")],string="Nature",required=True),
              "date_etablissement":fields.date("Date d'établissement",required=True),
              "autorite":fields.char("Autorité",size=128),
              }
hr_piece_identite()

class hr_contract(osv.osv): 
    def calcul_anciennete_actuel(self,cr,uid,ids,context=None):
        contracts=self.browse(cr, uid, ids, context=context)
        anciennete={}
        for contract in contracts:
            date_debut=datetime.strptime(contract.date_start,'%Y-%m-%d')
            today = datetime.now()
            result=today - date_debut
            if contract.date_end :
                date_end = datetime.strptime(contract.date_end,'%Y-%m-%d')
                today = datetime.now()
                if today > date_end :
                    result = date_end - date_debut
#            else :
#                result = today - date_debut
            nombre_day=result.days
            nbre_year=int(nombre_day/365)
            nbre_year=contract.an_report+nbre_year
            jour_restant=result.days%365
            nbre_mois=jour_restant/30
            nbre_mois=nbre_mois+contract.mois_report
            if nbre_mois >= 12:
                nbre_year=nbre_year+int(nbre_mois/12)
                nbre_mois=nbre_mois%12
            anciennete={
                        'year_old':nbre_year,
                        'month_old':nbre_mois,
                        }
        return anciennete
    
    def _get_annee_actuel(self,cr,uid,ids,field,arg,context=None):
        res={}
        contract_list=self.browse(cr,uid,ids,context)
        anciennete=self.calcul_anciennete_actuel(cr, uid,ids, context=context)
        for contract in contract_list :
                res[contract.id] = anciennete['year_old']
        return res
    
    def _get_mois_actuel(self,cr,uid,ids,field,arg,context=None):
        res={}
        contract_list=self.browse(cr,uid,ids,context)
        anciennete=self.calcul_anciennete_actuel(cr, uid,ids, context=context)
        for contract in contract_list :
                res[contract.id] = anciennete['month_old']
        return res
		
		
    def _get_brut(self,cr,uid,ids,field,arg,context=None):
        res={}
        contract_list=self.browse(cr,uid,ids,context)
        for contract in contract_list :
                res[contract.id] = contract.wage + contract.sursalaire
        return res
    
    def get_anciennete(self,cr,uid,ids,date_start,context=None):
        res = {'value':{
                      'anne_anc':0,
                      }
            }
        contracts=self.browse(cr, uid, ids, context=context)
        for contract in contracts:
            date_debut=datetime.strptime(contract.date_start,'%Y-%m-%d')
            today = datetime.now()
            result=False
            if contract.date_end :
                date_end = datetime.strptime(contract.date_end,'%Y-%m-%d')
                today = datetime.now()
                if today > date_end :
                    result = date_end - date_debut
            else :
                result = today - date_debut
            nombre_day=result.days
            nbre_year=int(nombre_day/360)
            nbre_year=contract.an_report+nbre_year
            jour_restant=result.days%360
            nbre_mois=jour_restant/30
            nbre_mois=nbre_mois+contract.mois_report
            if nbre_mois >= 12:
                nbre_year=nbre_year+int(nbre_mois/12)
                nbre_mois=nbre_mois%12
            res['value'].update({
                             'anne_anc':nbre_year,
                             })
        return res
    

        
    _inherit="hr.contract"
    _columns={
              'expatried':fields.boolean('Expatrié'),
              'an_report':fields.integer('Année',size=2),
              'mois_report':fields.integer('Mois report'),
              'an_anciennete':fields.function(_get_annee_actuel,method=True, type='integer', 
                                              string="Nombre d'année"),
              'mois_anciennete':fields.function(_get_mois_actuel,method=True, type='integer', 
                                                string="Nombre de mois"),
              #'anciennete_actuelle':fields.function(_get_annee_actuel,type='char',size=64),
              'anne_anc':fields.integer('Année'),
              "categorie_id":fields.many2one("hr.categorie.contract",'Catégorie'),
              "sursalaire":fields.integer('SurSalaire',required=True),
              "hr_convention_id":fields.many2one('hr.convention',"Convention",required=True),
              "hr_secteur_id":fields.many2one('hr.secteur.activite',"Secteur d'activité",required=True),
              'categorie_salariale_id':fields.many2one('hr.categorie.salariale', 'Catégorie salariale', required=True),
              "hr_payroll_prime_ids":fields.one2many("hr.payroll.prime.montant",'contract_id',"Primes"),
              'state':fields.selection([
                    ('draft','Draft'),('in_progress',"En cours"),('ended','Terminé'),('cancel','Annulé'),
                     ],'Eat du contrat', select=True, readonly=True),
              "type_ended":fields.selection([('licenced','Licencement'),
                ('hard_licenced','Licencement faute grave'),
                ('ended','Fin de contract'),], 'Type de clôture', select=True),
              "description_cloture":fields.text("Motif de Clôture"),
#               "hr_contract_closed_id":fields.many2one("hr.contract.closed","Cloture"),
              'wage':fields.integer('Salaire de base',required=True),
			  'arrive_emploi': fields.date('Date d’arrivée dans l’emploi'),
			  'afectation_poste': fields.date('Date d’affectation sur le poste'),
              'brut':fields.function(_get_brut,method=True, type='integer', 
                                              string="Salaire brut"),
              }
    _defaults={
               'expatried':False,
               "state":'draft',
               }
    
    def validate_contract(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'in_progress'})
    
    def closing_contract(self,cr,uid,ids,context=None):
        view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'hr_contract_extension', 'hr_contract_closed_form_view')
        #raise osv.except_osv('Test',view_id)
    
        return {
                'name':_("Clôture de contrat"),
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'hr.contract.closed',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': context,
            }
        
    def action_cancel(self,cr,uid,ids,context=None):
        return self.write(cr, uid, ids, {'state':'cancel'}, context)
    
    def on_change_convention_id(self,cr,uid,ids,hr_convention_id,context=None):
        if hr_convention_id :
            return {'domain':{'hr_secteur_id':[('hr_convention_id','=',hr_convention_id)]}}
        else :
            return {'domain':{'hr_secteur_id':[('hr_convention_id','=',False)]}}
        
    def on_change_secteur_id(self,cr,uid,ids,hr_secteur_id,context=None):
        if hr_secteur_id :
            return {'domain':{'categorie_salariale_id':[('hr_secteur_activite_id','=',hr_secteur_id)]}}
        else :
            return {'domain':{'categorie_salariale_id':[('hr_secteur_activite_id','=',False)]}}
        
        
    def change_categorie(self,cr,uid,ids,categorie_id,context=None):
        res = {'value':{
                      'wage':0,
                      }
            }
        categorie_obj=self.pool.get("hr.categorie.salariale")
        categorie=categorie_obj.browse(cr,uid,categorie_id,context=context)
        res['value'].update({
                             'wage':categorie.salaire_base,
                        })
        return res
hr_contract()


class hr_payroll_prime(osv.osv):
    _name = "hr.payroll.prime"
    _description = "prime"
    _columns={
              'name':fields.char('name', size=64,required=True),
              'code':fields.char('Code', size=64, required=True),
              'description':fields.text('Description'),
              }
hr_payroll_prime()

class hr_payroll_prime_montant(osv.osv):
    
    def _get_code_prime(self,cr,uid,ids,field,arg,context=None):
        res={}
        payroll_prime=self.browse(cr, uid, ids, context=context)
        for prime in payroll_prime:
            code=prime.prime_id.code
            res[prime.id]=code
        return res
    
    _name="hr.payroll.prime.montant"
    _columns={
              "prime_id":fields.many2one('hr.payroll.prime','prime',required=True),
              "code":fields.function(_get_code_prime,method=True, type='char', string="Code"),
              "contract_id":fields.many2one('hr.contract','Contract'),
              "montant_prime":fields.integer('Montant', required=True),
              }
hr_payroll_prime_montant()


class hr_contract_type(osv.osv):
    _inherit="hr.contract.type"
    _name="hr.contract.type"
    _columns={
              'code':fields.char("Code",siez=5,required=True),
              }
hr_contract_type()
