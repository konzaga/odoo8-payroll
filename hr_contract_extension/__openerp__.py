# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_synthese
# ##############################################################################
{
    "name" : "Extension du contrats",
    "version" : "1.0",
    "author" : "VEONE Technologies",
    'category': 'Human Resources',
    "website" : "www.veone.net",
    "depends" : ["hr_contract"],
    "description": """ Extension du contrats de trvail des employés
    """,
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
            "security/ir.model.access.csv",
            "data/primes_data.xml",
            "wizard/hr_contract_closed.xml",
            "res_country_view.xml",
            "hr_employee_view.xml",
            "hr_convention_view.xml",  
            "hr_contract_view.xml",     
        ],
    "data":[
           
            ],
    "installable": True
}
