# -*- coding: utf-8 -*-


import time
from openerp.osv import osv,fields
from datetime import datetime

class convention(osv.osv):
    _name="hr.convention"
    _description="Convention"
    _columns={
              "name":fields.char("Name",size=128,required=True),
              "description":fields.text("Description"),
              "hr_secteur_ids":fields.one2many("hr.secteur.activite","hr_convention_id","Secteurs d'activtés"),
              }  
convention()

class hr_secteur_activite(osv.osv):
    _name="hr.secteur.activite"
    _description = "Secteur d'activite"
    _columns = {
            "name":fields.char("Name",size=128,required=True),
            "description":fields.text("Description"),
            "hr_convention_id":fields.many2one("hr.convention","Convention",required=True),
            "hr_categorie_salariale_ids":fields.one2many("hr.categorie.salariale","hr_secteur_activite_id","Catégories salariales"),
        } 
    
hr_secteur_activite()


class categorie_salariale(osv.osv):
    _name = "hr.categorie.salariale"
    _description = "Categorie salariale"

    _columns={
              'name':fields.char('Libellé', size=64, required=False, readonly=False),
              'salaire_base':fields.float('Salaire de base'),
              'description':fields.text('Description'),
              'hr_secteur_activite_id':fields.many2one("hr.secteur.activite","Secteur d'activté"),
			  'hr_job_tranche_id':fields.many2one("hr.job","job_salary"),
              }
    
    
    def write(self, cr, user, ids, vals, context=None): 
        for salarie in self.browse(cr, user, ids, context=context):
            for contract in salarie.contract_ids:   
                #raise osv.except_osv(_('TEST'),contract)    
                cr.execute('UPDATE hr_contract set wage=%s WHERE id=%s',(salarie.salaire_base,contract.id))
                cr.commit()
        super(categorie_salariale,self).write(cr, user, ids, vals, context)
        return True
categorie_salariale()
    
