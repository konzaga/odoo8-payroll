# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2014 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_payroll_ci
# ##############################################################################

from openerp.osv import osv, fields

class res_country(osv.osv):
    _inherit='res.country'
    
    _columns={
              'nationalite':fields.char('Nationalité', size=64, required=False, readonly=False),
              }
    
res_country()


class res_city(osv.osv):
    _name="res.ville"
    _description='Ville'
    
    _columns={
              'name':fields.char('Ville', size=64, required=False, readonly=False),
              'country_id':fields.many2one('res.country', 'Pays', required=False), 
              }
res_city()

class res_commune(osv.osv):
    _name="res.commune"
    _description='Commune'
    
    _columns={
              'name':fields.char('Ville', size=64, required=False, readonly=False),
              'ville_id':fields.many2one('res.ville', 'Ville', required=False), 
              }
res_city()


class res_quartier(osv.osv):
    _name="res.quartier"
    _description='Quartier'
    
    _columns={
              'name':fields.char('Quartier', size=64, required=False, readonly=False),
              'commune_id':fields.many2one('res.commune', 'Commune', required=False), 
              }
res_city()


class res_partner(osv.osv):
    _inherit='res.partner'
    
    
    def onchange_ville_id(self, cr, uid, ids, ville_id):
        if not ville_id :
            return {}
        ville = self.pool.get('res.ville').browse(cr, uid, ville_id)
        return {
                'value' : {
                           'city': ville.name
                           }
                }
    
    
    _columns={
              'ville_id':fields.many2one('res.ville', 'Ville', required=False),
              'commune_id':fields.many2one('res.commune', 'Commune', required=False), 
              'quartier_id':fields.many2one('res.quartier', 'Quartier', required=False), 
              'ilot':fields.char('Ilot', size=64, required=False, readonly=False),
              'lot':fields.char('Lot', size=64, required=False, readonly=False),
              }
    
res_partner()
