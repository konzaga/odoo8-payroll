# -*- coding: utf-8 -*-


import time
from openerp.osv import osv,fields
from openerp.tools.translate import _
from datetime import datetime


class hr_contract_closed(osv.osv):
    _name="hr.contract.closed"
    _description = "contracts closed"
    _columns = {
            "name":fields.selection([
                ('licenced','Licencement'),
                ('hard_licenced','Licencement faute grave'),
                ('ended','Fin de contract'),
                 ], 'Name', select=True),
            "date_closing":fields.datetime("Date de clôture",required=True),
            "description":fields.text("Description",required=True),
            "date_create":fields.datetime("Date de creation"),
        } 
    _defaults = {
            'date_create': lambda *a: time.strftime('%Y-%m-%d'),   
        }
    
    def cloture_contract(self,cr,uid,ids,context=None):
        hr_contract_id=context.get('active_ids')
        hc_obj=self.pool.get('hr.contract')
        for i in self.browse(cr, uid, ids, context):
            hc_obj.write(cr,uid,hr_contract_id,{'date_end':i.date_closing,
                                                'description_cloture':i.description,
                                                'type_ended':i.name,
                                                'state':'ended',})
        return True
        
hr_contract_closed()