# -*- coding: utf-8 -*-
# Copyright 2017 Elico Corp (www.elico-corp.com).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import models, fields


class HrPayrollReport(models.Model):
    _inherit = 'hr.payslip.line'
    # _columns={
    #         'slip_id.date_from':fields.date('Date de début',required=True),
    #         'slip_id.date_to':fields.date('Date de fin',required=True),
    #     }
    date_from = fields.Date(related='slip_id.date_from',string='Date Début', store=True)
    #date_from = fields.date('Date de début',required=True)
    #date_to = fields.date('Date de fin',required=True)
    date_to = fields.Date(related='slip_id.date_to', string='Date Fin', store=True)
    """payslip_run_id = fields.Char('Payslip Batches',
                                 related='slip_id.payslip_run_id.name',
                                 store=True)"""
    employee_name=fields.Char('Nom employé',related='slip_id.employee_id.name', store=True)
    # _defaults = {  
    #     'slip_id.date_from': lambda *a: time.strftime('%Y-%m-01'),  
    #     'slip_id.date_to': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
    #     }
    #employee_name=fields.integer('Employe name', 'slip_id.employee_id.name', store=True)
    #print(payslip_run_id)
    #employe_ids=fields.one2many('hr.employe','employe_id',"Nom employe")