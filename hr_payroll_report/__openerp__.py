# -*- coding: utf-8 -*-
# Copyright 2017 Elico Corp (www.elico-corp.com).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Payslip Lines report",
    "version": "8.0.1",
    "summary": "Analyze your payroll with the Payslip Lines report",
    "category": "Generic Modules/Human Resources",
    "website": "https://www.wegnontechnologies.com/",
    "author": "Coulibaly Konzaga Apollinaire",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        "hr_payroll",
    ],
    'data': [
        'views/hr_payroll_report_menu.xml',
        'views/hr_payslip_line_view.xml',
    ],
}
