# -*- coding: utf-8 -*-
##############################################################################
##############################################################################


import time
import math
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _


from openerp.tools.safe_eval import safe_eval as eval


class hr_type_attribution_holidays(osv.osv):
    _name="hr.type.attribution.holidays"
    _description = "Type d'attribution de conges"
    _columns = {
                "name":fields.char("Libellé",size=128,required=True),
                'code':fields.char('Code', size=3, required=True),
                "taux":fields.float("Taux de calcul",required=True),
            } 
hr_type_attribution_holidays()

class hr_days(osv.osv):
    _name="hr.days"
    _description = 'Les jours'
    _columns = {
            'name':fields.char('Libéllé',size=128,required=True),
            'sequence':fields.integer('Sequence',required=True),
        } 
hr_days()

class hr_holidays_days(osv.osv):
    _name="hr.holidays.days"
    _description="The holidays "
    _columns = {
        "name":fields.char("Name",size=128,required=True),
        "date_holidays":fields.date("Date"),   
        "description":fields.text("Description"), 
        }
hr_holidays_days()

class resource_calandar(osv.osv):
    _inherit = "resource.calendar" 
    _columns = {
           'days_ids':fields.many2many('hr.days', 'resource_days_rel', 'resource_id', 'days_id', 'Label'), 
        }
resource_calandar()



class hr_holidays_status(osv.osv):
    
    def __init__(self,pool,cr):
        super(hr_holidays_status,self).__init__(pool,cr)
        cr.execute("DELETE FROM hr_holidays_status WHERE id in (1,2,3,4)")
        cr.commit()
        
    _inherit = 'hr.holidays.status'
    _columns = {
            'code':fields.char("Code",siez="5",required=True),
            'number_of_days':fields.integer("Nombre de jours"),
        } 
hr_holidays_status()


class hr_holidays(osv.osv):
    
    _inherit = 'hr.holidays'
    
    def create(self,cr,uid,vals,context=None):
        hremp_obj=self.pool.get('hr.employee')
        hhstatus_obj=self.pool.get('hr.holidays.status')
        employee=hremp_obj.browse(cr,uid,vals['employee_id'],context)
        status=hhstatus_obj.browse(cr,uid,vals['holiday_status_id'],context)
#         if status.code =='CONG' and employee.contract_id.an_anciennete<1 and vals['type']=='add':
#             raise osv.except_osv('Attention',"Cet employé ne peut pas bénéficier de ce type de congés")
        if status.number_of_days < vals['number_of_days_temp'] and status.number_of_days !=0:
            raise osv.except_osv('Attention',_("Le nombre de jours de congés pour ce type ne doit pas depassé %s"%status.number_of_days))
        date_attribution=time.strftime('%Y-%m-01')[:4]
        vals.update({"year_attribution":date_attribution})
        print(vals)
        return super(hr_holidays,self).create(cr,uid,vals,context)
    
    def get_employee_for_leave(self,cr,uid,ids,context=None):
        he_obj=self.pool.get("hr.employee")
        cr.execute("SELECT id FROM hr_employee")
        he_ids=[]
        results=cr.fetchall()
        if results :
            he_ids=[res[0] for res in results]
        hr_employee=he_obj.browse(cr,uid,he_ids,context)
        list_employee=[]
        for emp in hr_employee:
            if emp.contract_id and emp.contract_id.an_anciennete>=1:
                list_employee+=[emp.id]
        if not list_employee:
            raise osv.except_osv('Attention',"Il faut qu'un de vos employés ait au moins un an d'anciennété")
        else :
            return {'domain':{'employee_id':[('id','in',list_employee)]}}
    
    def get_all_employee(self,cr,uid,ids,context=None):
        he_obj=self.pool.get("hr.employee")
        cr.execute("SELECT id FROM hr_employee")
        he_ids=[]
        results=cr.fetchall()
        if results :
            he_ids=[res[0] for res in results]
        return {'domain':{'employee_id':[('id','in',he_ids)]}}
    
    def _get_year_attribution(self,cr,uid,ids,args,field_name,context=None):
        raise osv.except_osv('Test',time.strftime('%Y-%m-01'))
    

    
    def _get_number_of_hours(self, cr,uid,ids,date_from, date_to):
        """Returns a float equals to the timedelta between two dates given as string."""
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        from_dt = datetime.datetime.strptime(date_from, DATETIME_FORMAT)
        to_dt = datetime.datetime.strptime(date_to, DATETIME_FORMAT)
        timedelta = to_dt - from_dt
        diff_day = float(timedelta.seconds)/3600
        return diff_day
    
    def get_number_of_day_by_periode(self,cr,uid,ids,employee_id,date_from,date_to):
        hld_res=[]
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        employee=self.pool.get('hr.employee').browse(cr,uid,employee_id)
        days_ids=employee.contract_id.working_hours.days_ids
        from_dt = datetime.strptime(date_from, DATETIME_FORMAT)
        to_dt = datetime.strptime(date_to, DATETIME_FORMAT)
        timedelta = to_dt - from_dt
        day_temp = timedelta.days + float(timedelta.seconds) / 86400
        diff_day = int(round(math.floor(day_temp))+1)
        total_day=0
        number_of_day=0
        if (employee_id and date_from and date_to):
            cr.execute("select SUM(number_of_days_temp) from hr_holidays where employee_id=%s and type='remove' and state='validate'"
                " and (date_from between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))"
                " and (date_to between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))",(employee_id,date_from,date_to,date_from,date_to,))
            results=cr.fetchall()
            if results :
                number_of_day+=results[0]
            
            cr.execute("select id as id from hr_holidays where employee_id=%s and type='remove' and state='validate'"
                       " and (date_from between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) and "
                       "(date_to >to_date(%s,'yyyy-mm-dd'))",(employee_id,date_from,date_to,date_to))
            results=cr.fetchall()
            if results :
                print("ok")
            else :
                raise osv.except_osv('Testons',results)
        
    _columns = {
                'number_of_hours': fields.float("Nombre d'heures"),
                'year_attribution':fields.char("Année",size=10),
            }
hr_holidays()



class hr_employee(osv.osv):
    _inherit="hr.employee"
    
    def calcul_remaing_days(self,cr,uid,ids,context=None):
        date_attribution=time.strftime('%Y-%m-%d')
        holiday_obj=self.pool.get('hr.holidays')
        hr_emp_obj = self.pool.get('hr.employee')
        holiday_status_obj = self.pool.get('hr.holidays.status')
        company_obj = self.pool.get('res.company')
        hr_emp_ids=hr_emp_obj.search(cr,uid,[('date_next_attribution','=',date_attribution)])
        company=company_obj.browse(cr,uid,1,context)
        holiday_status=holiday_status_obj.search(cr,uid,[('code','=','CONG')])
        for emp in self.browse(cr,uid,hr_emp_ids,context):
            values={
                    'type':'add',
                    'number_of_days_temp':company.base_holidays,
                    'employee_id':emp.id,
                    'name':'Attribution de jours de congés pour le mois de Test',
                    'holiday_status_id':holiday_status[0],
                    'state':'validate',
                }
            holidays_id=holiday_obj.create(cr,uid,values,context)
            holiday_obj.write(cr,uid,holidays_id,{'state':'validate'})
            date_next_attribution=str(datetime.now() + relativedelta.relativedelta(months=+1))[:10]
            self.write(cr,uid,emp.id,{'date_next_attribution':date_next_attribution})
            
            
            
    
    def get_holiday_status(self,cr,uid,ids,context=None):
        hhs_obj=self.pool.get("hr.holidays.status")
        hh_obj=self.pool.get("hr.holidays")
        date_attribution=time.strftime('%Y-%m-01')[:4]
        hh_status_id=hhs_obj.search(cr,uid,[('code','=','CONG')])
        hholiday_id=hh_obj.search(cr,uid,[('year_attribution','=',date_attribution),('type','=','add'),
                ('state','=','validate'),('holiday_status_id','in',hh_status_id),("employee_id",'=',ids[0])])
        if hholiday_id:
            hholiday=hh_obj.browse(cr,uid,hholiday_id[0],context)
            return hholiday.holiday_status_id
        else : 
            return False
    
    def _get_max_leave(self,cr,uid,ids,arg, field_name,context=None):
        res={}
        hhs_obj=self.pool.get("hr.holidays.status")
        for i in self.browse(cr,uid,ids,context):
            hh_status=self.get_holiday_status(cr,uid,ids,context)
            if hh_status :
                res[i.id]=hh_status.max_leaves
        return res
    
    def _get_taken_leave(self,cr,uid,ids,arg, field_name,context=None):
        res={}
        hhs_obj=self.pool.get("hr.holidays.status")
        for i in self.browse(cr,uid,ids,context):
            hh_status=self.get_holiday_status(cr,uid,ids,context)
            if hh_status :
                res[i.id]=hh_status.remaining_leaves
        return res
    
    
    def get_date_retour_conges(self,cr,uid,ids,context=None):
        holidays_obj = self.pool.get('hr.holidays')
        hhs_obj=self.pool.get("hr.holidays.status")
        hol_status_id=hhs_obj.search(cr,uid,[('code','=','CONG')])[0]
        date_from = time.strftime('%Y-%m-01')
        date_to=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10]
        for emp in self.browse(cr, uid, ids, context):
            holidays_ids=holidays_obj.search(cr,uid,[('employee_id','=',emp.id),('holiday_status_id','=',hol_status_id)
                                                     ,('type','=','remove'),('state','=','validate'),('date_to','<=',str(datetime.now())[:10]),
                                                     ('date_to','<',date_from)],order='date_to')
            if holidays_ids:
                holiday=holidays_obj.browse(cr,uid,holidays_ids[len(holidays_ids)-1],context)
                return holiday.date_to
            else :
                return emp.date_embauche
    
    def _get_date_retour_conges(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for emp in self.browse(cr,uid,ids,context):
            res[emp.id]=self.get_date_retour_conges(cr, uid, ids, context)
        return res
            
    
    def get_montant_by_periode_reference(self,cr,uid,ids,context=None):
        slip_obj = self.pool.get('hr.payslip')
        slip_ref_ids=[]
        montant = 0
        for emp in self.browse(cr,uid,ids,context):
            slip_ids=slip_obj.search(cr,uid,[('employee_id','=',emp.id)])
            if slip_ids :
                payslip=slip_obj.browse(cr,uid,slip_ids,context)[0]
                slip_ref_ids+=slip_ids
                slip_ids=slip_obj.search(cr,uid,[('date_from','>',payslip.date_to),('employee_id','=',payslip.employee_id.id)])
                slip_ref_ids+=slip_ids
                for slip in slip_obj.browse(cr,uid,slip_ids,context):
                    for line in slip.line_ids :
                        if line.code == 'BRUT':
                            montant +=line.total
                if montant !=0 and slip_ids!=[]:
                    montant = montant/len(slip_ids)
            else :
                montant = 0
        return montant
    
    def _get_montant_by_periode_reference(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        for emp in self.browse(cr,uid,ids,context):
            res[emp.id]=self.get_montant_by_periode_reference(cr,uid,ids,context)
        return res
    
    _columns = {
                'date_next_attribution': fields.date('Date prochaine attribution'), 
                "max_leaves":fields.function(_get_max_leave,method=True,type="integer",string="Total jours congés"),
                "taken_leaves":fields.function(_get_taken_leave,method=True,type="integer",string="Total jours congés pris"),
                "montant_moyen_mensuel":fields.function(_get_montant_by_periode_reference,method=True,type='float',string="Montant mensuel moyen"),
            }
    
hr_employee()


