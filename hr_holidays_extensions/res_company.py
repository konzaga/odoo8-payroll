# -*- coding: utf-8 -*-

from openerp import api, models, fields


class res_company(models.Model):
    _inherit = 'res.company' 
    
    base_holidays = fields.Float('Nombre de jours/Mois', required=True)
    
    _defaults = {  
            'base_holidays':2.2,  
        }
    


