# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_synthese
# ##############################################################################
{
    "name" : "Extension des congés",
    "version" : "1.0",
    "author" : "VEONE Technologies",
    'category': 'Human Resources',
    "website" : "www.veone.net",
    "depends" : ["hr_holidays"],
    "description": """ Extension du contrats de trvail des employés
    """,
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
        "security/ir.model.access.csv",
        "hr_holidays_extension_view.xml",
        "res_company_view.xml",
        "res_config_view.xml",
        ],
    "data":[
            'hr_holidays_data.xml',
            ],
    "installable": True
}
