# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class human_resources_configuration(osv.osv_memory):
    _inherit = 'hr.config.settings'
    _columns = {
            'module_hr_calcul_holidays_planning': fields.boolean("Calcul des congés basé sur le planning de lémployé",
                help ="""Permet de choisir la base de calcul des congés"""),
            'module_hr_holidays_rattrapage_conge':fields.boolean('Rattrapage de congés',
                help="""Permettre aux employés de pouvoir rattraper les jours de congés restants 
                sur l'année suivante"""),
            'base_mensuel':fields.float("Nombrre de jours par mois",required=True),
        }
    
    _defaults = {  
        'module_hr_calcul_holidays_planning': True,  
        'base_mensuel':2.23,
        }
    


human_resources_configuration()


    
