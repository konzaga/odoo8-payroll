# -*- coding: utf-8 -*-
import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp.exceptions import except_orm, Warning, ValidationError,AccessError
import base64
from openerp.tools.translate import _
from openerp.tools import html2plaintext

from openerp import models, api, fields

import re
import logging
from openerp.tools import plaintext2html
#from pygments.lexer import _inherit

_logger = logging.getLogger(__name__)

class hr_employee(models.Model):
    _inherit = 'hr.employee'
    
    @api.onchange('date_embauche')
    def on_change_date_embauche(self):
        if self.date_retour_conge :
            if self.date_retour_conge < self.date_embauche :
                self.date_retour_conge = self.date_embauche
        else :
            self.date_retour_conge = self.date_embauche
                
        
    date_retour_conge = fields.Date(string="Date de rétour congé")
    
    
class hr_holidays(models.Model):
    
    _inherit = "hr.holidays"
    
    state_payroll = fields.Selection([('payed','Payé'),('not_payed','Pas payé')],string="Etat de paie",default='not_payed')
    
