# -*- coding: utf-8 -*-
##############################################################################
##############################################################################


import time
import math
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from collections import namedtuple

from openerp.tools.safe_eval import safe_eval as eval


from openerp import netsvc
from openerp.exceptions import Warning

from decimal import Decimal
from math import fabs, ceil, floor
import openerp.addons.decimal_precision as dp

class hr_payslip(osv.osv):
    _inherit = 'hr.payslip'

    _columns = {
                "is_conges":fields.boolean("Est Congés"),
            }

class hr_employee(osv.osv):
    _inherit = 'hr.employee'


    # def get_montant_by_periode_reference(self,cr,uid,ids,context=None):
    #     slip_obj = self.pool.get('hr.payslip')
    #     holi_obj = self.pool.get('hr.holidays')
    #     slip_ref_ids=[]
    #     montant = 0
    #     for emp in self.browse(cr,uid,ids,context):
    #         slip_ids=slip_obj.search(cr,uid,[('employee_id','=',emp.id)])
    #         # holi_ids= holi_obj.search(cr,uid,[('employee_id','!=',emp.id)])
    #         if slip_ids:
    #             payslip=slip_obj.browse(cr,uid,slip_ids,context)[0]
    #             slip_ref_ids+=slip_ids
    #             slip_ids=slip_obj.search(cr,uid,[('date_from','>',payslip.date_to),('employee_id','=',payslip.employee_id.id)])
    #             slip_ref_ids+=slip_ids
    #             for slip in slip_obj.browse(cr,uid,slip_ids,context):
    #                 for line in slip.line_ids :
    #                     if line.code == 'BRUT':
    #                         montant +=line.total
    #             if montant !=0 and slip_ids!=[] and emp.remaning_leaves :
    #                 montant = montant/len(slip_ids)
    #         else :
    #             montant = 0
    #     return montant
    
    def _get_montant_by_periode_reference(self,cr,uid,ids,field_name,arg,context=None):
        res={}
        slip_obj = self.pool.get('hr.payslip')
        for emp in self.browse(cr,uid,ids,context):
            slip_ref_ids=[]
            montant = 0
            slip_ids=slip_obj.search(cr,uid,[('employee_id','=',emp.id),('is_conges','=',False)])
            slip_ref_ids+=slip_ids
            slips = slip_obj.browse(cr, uid, slip_ids)
            # list_payslips=emp.slip_ids 
            date_debut = False
            if emp.date_retour_conge:
                date_debut = datetime.strptime(emp.date_retour_conge,'%Y-%m-%d')
            liste_date_debut = []
            liste_date_fin = []
            liste_date_debut2 = []
            for slip in slips:
                date_debut = datetime.strptime(slip.date_from,'%Y-%m-%d')
                liste_date_debut.append(date_debut)
            # for slip in slips:
                # date_debut2 = datetime.strptime(slip.contract_id.date_start,'%Y-%m-%d')           
                # liste_date_debut2.append(date_debut2)
                date_fin = datetime.strptime(slip.date_to,'%Y-%m-%d')
                liste_date_fin.append(date_fin)
                # print liste_date_debut

            # for slip in slips:
                # Range = namedtuple('Range',['start','end'])
                # r1=Range(start=date_debut,end=date_fin)
                # new_list=[]
                # if (len(list_payslips)!=1):
                #     for slipess in list_payslips:
                #         if slipess.id != slip.id :
                #             new_list.append(slipess)
                #     for slipess in new_list:
                #         old_date_from=datetime.strptime(slipess.date_from,'%Y-%m-%d')
                #         old_date_to = datetime.strptime(slipess.date_to,'%Y-%m-%d')
                #         r2=Range(start=old_date_from,end=old_date_to)
                #         result = max(r1.end, r2.end) - min(r1.start,r2.start)
                #         nombre_day=result.days
                #         nbre_mois=int(nombre_day/30)

                for line in slip.line_ids:
                    if line.code == 'BRUT':
                            montant +=line.total
            if liste_date_fin!=[]:
                delta = max(liste_date_fin)
            else:
                delta= 0
            # gama = min(liste_date_debut)
            if emp.date_retour_conge:
                gama = date_debut
            elif liste_date_debut!=[]:
                gama = min(liste_date_debut)
            else:
                gama = 0

            # print delta
            if liste_date_fin ==[] and liste_date_debut ==[]:
                nombre_day =0
            else:
                nombre_day=(delta - gama).days
            nbre_mois=int(nombre_day/30)
            # print nbre_mois
            if montant !=0 and slip_ids!=[]:
                    montant = montant/nbre_mois
                    res[emp.id] = montant
        return res







        #         res[emp.id]=self.get_montant_by_periode_reference(cr,uid,ids,context)
        # return res

    # def _get_montant_by_periode_reference(self,cr,uid,ids,field_name,arg,context=None):
    #     res={}
    #     for emp in self.browse(cr,uid,ids,context):
    #             res[emp.id]=self.get_montant_by_periode_reference(cr,uid,ids,context)
    #     return res

    _columns = {
                "montant_moyen_mensuel":fields.function(_get_montant_by_periode_reference,method=True,type='float',string="Montant mensuel moyen"),
                "date_retour_conge": fields.date(string="Date de rétour congé"),
            }

    # _defaults = {
    #         "date_retour_conge": lambda *a: time.strftime('2010-01-01'),
    #          }
