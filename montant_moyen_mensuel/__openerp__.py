# -*- encoding: utf-8 -*-

{
   'name' : 'Montant moyen mensuel',
   'version' : '1.0',
   'author' : 'afica performance',
   'description' : 'autre client',
   'category': 'Enterprise Innovation',
   'website': '',
   'depends' : ['ap_hr_holidays_ext_conges'], # liste des module a appeller lor d'un heritage (nom du model parent) ()
   'data' : ['montant_moyen_mensuel.xml'],# les fichiers de données à charger lors de l'installation du module
}