##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_emprunt
# ##############################################################################
{
    "name" : "Emprunt",
    "version" : "1.0",
    "author" : "VEONE",
    "category" : "Generic Modules/Human Resources",
    "website" : "www.veone.net",
    "depends" : ['hr',
                 'hr_payroll',
                 ],
    "description": """ Module permettant de gérer les emprunts des employés 
(Echeanciers, Remboursement, interfaçage avec le module de paie)
    """,
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
					"security/groups.xml",
                    "hr_emprunt_view.xml",
                    "security/ir.model.access.csv",
                    "hr_demande_emprunt_view.xml",
                    "report/report.xml",
                    "workflow/wkf_emprunt.xml",
                    "workflow/wkf_demande_emprunt.xml",
                    "wizard/create_echeance_view.xml",
                   ],
    "installable": True
}
