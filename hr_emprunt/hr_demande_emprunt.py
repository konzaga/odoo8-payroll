# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_emprunt
# ##############################################################################  -->
from openerp.osv import fields, osv
import datetime
import time

class hr_demande(osv.osv):
    _name = 'hr.emprunt.demande'
    _description = "Demande d'emprunt"
    
    def _employee_get(self, cr, uid, context=None):
        ids = self.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
        if ids:
            return ids[0]
        return False
    
    
    def onchange_employee_id(self, cr, uid, ids, employee_id):
        if not employee_id:
            return {}
        emp = self.pool.get('hr.employee').browse(cr, uid, employee_id)
        return {'value': { 
                            'name': ('Demande de %s') % (emp.name),
                            'job_id':emp.job_id.id,
                        },
                
                }
    
    
    def _get_quotite_cessible(self,cr,uid,ids,field,arg,context=None):
        res={}
        demandes=self.browse(cr,uid,ids)
        
        for demande in demandes :
            som = 0
            cr.execute('select somme_max from hr_emprunt_quotite where job_id=%s',(demande.job_id.id,))
            somme_max=cr.fetchall()
            for som in somme_max :
                res[demande.id]=som[0]
        return res
    
    def _check_quotite(self,cr,uid,ids,context=None):
        res={}
        demandes=self.browse(cr,uid,ids)
        for demande in demandes :
            if demande.somme_max > 0  and demande.somme_max < demande.montant_demande:
                raise osv.except_osv('Error', 'La quotitle cessible définie pour cet employé est inférieur à la somme demandée')
        return True
    
    
    _columns={
        'name' : fields.char("Libellé",size=150,required=True, states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'employe_id' : fields.many2one('hr.employee', 'Employe',ondelete='cascade', states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'job_id' : fields.many2one('hr.job', 'Poste',ondelete='cascade', states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'motif_demande' : fields.char("Motif",size=150,required=False, states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'montant_demande' : fields.float("Montant", size=20, required=True, states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'date_demande' : fields.date("Date d'emprunt", states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'date_echeance' : fields.date("Date d'échéance proposée", states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'somme_max':fields.function(_get_quotite_cessible,method=True, Type='float', string='Somme max'),
        'note':fields.text('Notes', states={'done':[('readonly',True)], 'cancel':[('readonly',True)]}),
        'state':fields.selection([('draft','Brouillon'),
                                  ('submitted','Soumis'),
                                  ('confirmed','Confirmé'),
                                  ('validated','Validé'),
                                  ('done','Clôturé'),
                                  ('cancel','Réfusée')],'Statut',readonly=True,required=False), 
              }
    
    #_constraints=[(_check_quotite,"",['montant_demande'])]
    
    def action_draft(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'draft'})
    
    def action_confirmed(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'confirmed'})
    
    def action_submitted(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'submitted'})
    
    def action_validated(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'validated'})
    
    def action_cancel(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'cancel'})
    
    def action_done(self,cr,uid,ids,context=None):
        emprunt_obj = self.pool.get('hr.emprunt')
        for demande in self.browse(cr,uid,ids):
            emprunt = {
                       'name':'Emprunt de %s' % (demande.employe_id.name),
                       'employee_id':demande.employe_id.id,
                       'date_emprunt':time.strftime('%Y-%m-%d'),
                       'montant_emprunt':demande.montant_demande,
                       'statut_emprunt':False,
                       'option':'lineaire',
                       'state':'draft',
                       'demande_id':demande.id,
            
                       }
            emprunt_obj.create(cr,uid,emprunt,context=context)
            
        return self.write(cr,uid,ids,{'state':'done'})
    
    _defaults = {
        'employe_id': _employee_get,
        'state': 'draft',
        'date_demande':time.strftime('%Y-%m-%d'),
    }
    
hr_demande()