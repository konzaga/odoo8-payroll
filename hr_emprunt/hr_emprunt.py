# -*- encoding: utf-8 -*-

##############################################################################
#
# Copyright (c) 2012 Veone - support.veone.net
# Author: Veone
#
# Fichier du module hr_emprunt
# ##############################################################################  -->
from openerp.osv import fields, osv
from datetime import datetime,date
import time
from openerp.tools.translate import _
from datetime import timedelta
from dateutil import relativedelta

class hr_employee(osv.osv):
    _name = "hr.employee"
    _description = "Employees' emprunts"
    _inherit = "hr.employee"
    
    def _get_echeance_count(self,cr,uid,ids,field,arg,context=None):
        res={}
        employe_list=self.browse(cr,uid,ids,context)
        for employe in employe_list :
            cpt=0
            mois=time.strftime('%m')
            annee=time.strftime('%y')
            for emprunt in employe.emprunt_ids :
                if emprunt.state == 'confirmed':
                    for echeance in emprunt.echeance_ids :
                        if echeance.date_remboursement_echeance[2:4]==annee :
                            if echeance.date_remboursement_echeance[5:7]==mois :
                                cpt+=echeance.montant_echeance
            res[employe.id]=cpt
        return res
    
    _columns = {
                    'dette' : fields.boolean('Dette a rembourser'),
                    'emprunt_ids' : fields.one2many('hr.emprunt', 'employee_id', 'Emprunts'),
                    'echeance_count':fields.function(_get_echeance_count,method=True, Type='integer', 
                                                     string='Somme totale due', domain='[()]',store=False),
               }
hr_employee()



class hr_emprunt(osv.osv):
    _name = 'hr.emprunt'
    _description = 'Emprunt des employés'
    
    def write(self,cr,user,ids,vals,context=None):
        res={}
        for emprunt in self.browse(cr,user,ids,context=context):
            echeances = emprunt.echeance_ids
            montant=0
            for ech in echeances:
                montant+=ech.montant_echeance
            if montant > emprunt.total_emprunt:
                raise osv.except_osv(_('ERREUR'),_("Le montant à remboursé doit être inférieur au total emprunté"))
            else :
                super(hr_emprunt,self).write(cr, user, ids, vals, context)
    
    def _get_count_echeances(self,cr,uid,ids,field,arg,context=None):
        res={}
        emprunt_list=self.browse(cr,uid,ids,context)
        for emprunt in emprunt_list:
            cpt=0
            for echeance in emprunt.echeance_ids:
                cpt += echeance.montant_echeance    
            res[emprunt.id]=cpt        
        return res
    
    def _get_remaining_emprunt(self,e,echeances):
        if e==0:
          return 0
      
        return(100*(e - (echeances))/e) or 0
    
    def _remaining_emprunt_percent(self,cr,uid,ids,field,arg,context=None):
        res={}
        var_tmp_ids=self.browse(cr,uid,ids,context)
        
        # Calcul de la somme des échéances remboursées
        for e in var_tmp_ids:
            cpt = 0
            for echeance in e.echeance_ids :
                if echeance.statut_echeance == True :
                    cpt += echeance.montant_echeance
            res[e.id]=self._get_remaining_emprunt(e.total_emprunt + (e.total_emprunt*e.taux/100),cpt)
        return res
    
    def onchange_employee_id(self, cr, uid, ids, employee_id):
        if not employee_id:
            return {}
        emp = self.pool.get('hr.employee').browse(cr, uid, employee_id)
        return {'value': {
                          'name': ('Emprunt de %s') % (emp.name)},
                }
    
    def onchange_echeances(self,cr,uid,ids,mtt_emprunt,echeances_list):
        res={}
        res['value']={
            'remaining_emprunt':self._get_remaining_emprunt(mtt_emprunt,echeances_list)
             }
        if mtt_emprunt<0:
            res['warning']={
                            'title':'Warning',
                            'message':'Veuillez entrer un montant positif',
                           }
        return res
    
    def _check_echeances(self,cr,uid,ids,context=None):
        res={}
        emprunts=self.browse(cr,uid,ids)
        for emprunt in emprunts :
            if emprunt.option == 'variee' and emprunt.montant_emprunt != emprunt.somme_echeances :
                return False
        return True
    
    def _check_employee_status(self,cr,uid,ids,context=None):
        emprunts=self.browse(cr,uid,ids)
        for emprunt in emprunts :
            if emprunt.option != 'lineaire' and len(emprunt.echeance_ids) == 0 :
                raise osv.except_osv('Error',"Veuillez définir des échéances pour le prêt")
            if emprunt.employee_id.job_id :
                cr.execute('select job_id, somme_min, somme_max from hr_emprunt_quotite')
                cotite = cr.fetchall()
                for cot in cotite :
                    if emprunt.employee_id.job_id==cot[0] :
                        raise osv.except_osv('Error',"La somme demandée pour ce prêt n'est pas définie dans la cotité cessible de cet employé")
        return True
    
    def on_change_something(self,cr,uid,ids,montant_emprunt,taux,context=None):
        res={}
        total_emprunt = (montant_emprunt + (montant_emprunt * taux/100))
        res['value']={
                        'total_emprunt':total_emprunt,
                        'echance_ids':[],
                        }
        return res
            
    def on_change_option(self,cr,uid,ids,option,context=None):
        res={}
        res['value']={
                          'echeance_ids':[]
                          }     
        return res

    def move_to_linear(self,cr,uid,ids,option,nbre_ech,total_emprunt,date_debut,context=None):
        res=[]
        test={} 
        if option == 'week':
            temp=datetime.strptime(date_debut,'%Y-%m-%d')
            for i in range(nbre_ech):
                if i==0:
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':total_emprunt/nbre_ech, 
                        'date_prevu':date_debut,
                        }
                    res+=[inputs]
                elif i==nbre_ech-1:
                    temp=temp + timedelta(days=+7)
                    somme=(total_emprunt/nbre_ech)*(i)
                    reste = total_emprunt-somme
                    temp= temp + timedelta(days=+7)
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':reste, 
                        'date_prevu':str(temp),
                        }
                    res+=[inputs]  
                else :
                    temp= temp + timedelta(days=+7)
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':total_emprunt/nbre_ech, 
                        'date_prevu':str(temp),
                        }
                    res+=[inputs]  
        if option == 'month':
            temp=datetime.strptime(date_debut,'%Y-%m-%d')
            for i in range(nbre_ech):
                if i==0:
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':total_emprunt/nbre_ech, 
                        'date_prevu':date_debut,
                        }
                    res+=[inputs]
                elif i==nbre_ech-1:
                    temp=temp + timedelta(days=+7)
                    somme=(total_emprunt/nbre_ech)*(i)
                    reste = total_emprunt-somme
                    temp= temp + timedelta(days=+7)
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':reste, 
                        'date_prevu':str(temp),
                        }
                    res+=[inputs]  
                else:
                    temp= temp + relativedelta.relativedelta(months=+1, day=1)
                    inputs={
                        'name':_('Echéance %s')%(i+1),
                        'montant_echeance':total_emprunt/nbre_ech, 
                        'date_prevu':str(temp),
                        }
                    res+=[inputs]                  
        test['value']={
                        'echeance_ids':res,
                    }
        return test
           
    _columns = {
        'name' : fields.char("Libellé de l'emprunt",size=150,required=True),
        'employee_id' : fields.many2one('hr.employee', 'Employe',required=True, ondelete='cascade'),
        'demande_id' : fields.many2one('hr.emprunt.demande', 'Demande',ondelete='cascade'),
        'echeance_ids' : fields.one2many('hr.echeance', 'emprunt_id', 'Echéances'),
        'montant_emprunt' : fields.float("Montant emprunt", size=20, required=True),
        'date_emprunt' : fields.date("Date d'emprunt"),
        'date_debut_remboursement':fields.date('Date debut remboursement',required=True),
        'date_echeance' : fields.date("Date d'échéance"),
        'statut_emprunt' : fields.boolean('Reglé'),
        'total_emprunt':fields.float('Total à rembourser'),
        'remaining_emprunt':fields.function(_remaining_emprunt_percent,method=True, Type='float', string='Taux remb. restant'),
        'taux':fields.float("Taux d'emprunt", help="Taux d'intérêt de remboursement"),
        'somme_echeances':fields.function(_get_count_echeances,method=True, Type='float', string='Somme des echeances'),
        'option':fields.selection([('lineaire','Linéaire'),
                                      ('variee','Variée')],'Option échéance',readonly=False,required=True),
		'nb_echeance':fields.integer("Nombre d'échéance(s)"),
        'intervalle_echeance':fields.selection([('week','Hebdomadaire'),('month','Mensuel')],'Intervalle',readonly=False),
        'notes':fields.text('Notes'),
        'state':fields.selection([('draft','Brouillon'),('confirmed','Confirm�'),
                                      ('done','Termin�')],'Status',readonly=True,required=True),
        }
    #_constraints=[(_check_echeances,"Le montant de l'emprunt doit être égal à la somme des échéances",['montant_emprunt','somme_echeances'])]
    _constraints=[(_check_employee_status,"",['montant_emprunt'])]
    
    _defaults={
              'state':'draft',
               'date_emprunt':time.strftime('%Y-%m-%d'),
               'date_debut_remboursement':time.strftime('%Y-%m-%d'),
               'option':'variee',
               'taux':0.00,
               'intervalle_echeance':'month',
               }
    
    def action_draft(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'draft'})
    
    def action_confirm(self,cr,uid,ids,context=None):
        echeance_obj=self.pool.get('hr.echeance')
        cpt=0
        intervalle=0

        for emprunt in self.browse(cr,uid,ids):
            if not emprunt.employee_id.job_id :
                raise osv.except_osv('Error',"Veuillez définir un poste pour l'employé demandeur du prêt")
            if emprunt.option == 'lineaire' :  
                if emprunt.nb_echeance == 0 :
                    raise osv.except_osv('Error',"Veuillez définir le nombre d'échéances")   
                if emprunt.intervalle_echeance == 0 :
                    raise osv.except_osv('Error',"Veuillez définir l'intervalle des échéances")   
                             
                date_from = datetime.datetime.strptime(emprunt.date_emprunt, '%Y-%m-%d')
                while cpt < emprunt.nb_echeance:
                    intervalle+=emprunt.intervalle_echeance
                    duration_delta = datetime.timedelta(0,intervalle * 24 * 60 * 60)
                    date_to = date_from + duration_delta
                    echeance_line = {
                                        'name':'Echeance %s' % (cpt+1),
                                        'montant_echeance':(emprunt.montant_emprunt + emprunt.montant_emprunt*emprunt.taux/100)/emprunt.nb_echeance,
                                        'statut_echeance':False,
                                        'date_remboursement_echeance':date_to,
                                        'emprunt_id':emprunt.id
                                     }
                    echeance_obj.create(cr, uid, echeance_line, context=context)
                    cpt+=1
                    if cpt >= emprunt.nb_echeance:
                        self.write(cr,uid,ids,{'date_echeance':date_to})
                        
        return self.write(cr,uid,ids,{'state':'confirmed'})
        
    def action_done(self,cr,uid,ids,context=None):
        return self.write(cr,uid,ids,{'state':'done'})

    def on_change_date_echance(self,cr,uid,ids,date_debut,date_echeance,total_emprunt,context=None):
        res=[] 
        test={}      
        date_deb = datetime.strptime('2014-10-06','%Y-%m-%d')
        date_fin = datetime.strptime('2014-10-06','%Y-%m-%d')
        if date_debut : date_deb=datetime.strptime(date_debut,'%Y-%m-%d')
        if date_echeance : date_fin = datetime.strptime(date_echeance,'%Y-%m-%d')
        total_days=(date_fin - date_deb).days
        nbre_echeance=int(total_days/30)
        #raise osv.except_osv(_('TEST'),date_debut.time())
        for i in range(nbre_echeance):
            if i==0:
                inputs={
                    'name':_('Echéance %s')%(i+1),
                    'montant_echeance':total_emprunt/nbre_echeance, 
                    'date_prevu':date_debut,
                    }
            elif i==nbre_echeance-1:
                nb=(i*30)
                temp =date_deb + timedelta(days=nb)
                somme = ((total_emprunt/nbre_echeance) * (i))
                reste = total_emprunt - somme
                inputs={
                    'name':_('Echéance %s')%(i+1),
                    'montant_echeance':reste, 
                    'date_prevu':str(date_fin),
                    }                 
            else:
                nb=(i*30)
                temp =date_deb + timedelta(days=nb)
                #raise osv.except_osv(_('TEST'),temp)
                inputs={
                    'name':_('Echéance %s')%(i+1),
                    'montant_echeance':total_emprunt/nbre_echeance, 
                    'date_prevu':str(temp),
                    }             
            res+=[inputs]
        test['value']={
                        'echeance_ids':res,
                    }
        return test

    def on_change_test(self,cr,uid,ids,test,context=None):
        for list in test :
            echeance = self.pool.get('hr.echeance').browse(cr,uid,list[0],context=context)
            raise osv.except_osv(_('TEST'),echeance)
hr_emprunt()


class echeance(osv.osv):
    _name = 'hr.echeance'
    _description = 'Echeances des emprunts'
    
    def _check_montant(self,cr,uid,ids,context=None):
        echeances=self.browse(cr,uid,ids)
        for echeance in echeances :
            if echeance.montant_echeance <= 0 :
                return False
        return True
    
    
    
    _columns = {
        'name' : fields.char('Libellé',size=60,required=True),
        'montant_echeance' : fields.float('Montant',size=60),
        'statut_echeance' : fields.boolean('Reglé'),
        'date_prevu':fields.date('Date prévu'),
        'date_remboursement_echeance' : fields.date('Date de remboursement'),
        'emprunt_id' : fields.many2one('hr.emprunt', 'Emprunt', readonly=True),

    }
    _constraints=[(_check_montant,"Le montant de l'échéance doit être supérieur à 0 ",['montant_echeance'])]
    
    def on_change_test(self,cr,uid,ids,test,context=None):
        for echeance in self.browse(cr, uid, ids, context=context):
            raise osv.except_osv(_('TEST'),echeance.emprunt_id)
    
echeance()

class hr_emprunt_quotite(osv.osv):
    _name = 'hr.emprunt.quotite'
    _description = 'Quotités cessibles des emprunts'
    
    _columns = {
                'name':fields.char('Désignation', size=64, required=False, readonly=False),
                'job_id':fields.many2one('hr.job', 'Poste', required=False),
                'somme_min':fields.float('Somme min'),
                'somme_max':fields.float('Somme max'),
                'description':fields.text('Description'),
                }
hr_emprunt_quotite()


class hr_payslip(osv.osv):
    _name = 'hr.payslip'
    _inherit = 'hr.payslip'
    
    """def _compute_emprunt(self, cr,uid,ids):
        emprunt_obj = self.pool.get('hr.emprunt')
        emprunt_ids = emprunt_obj.search(cr, uid, [('employee_id', '=', self.employee_id.id)], context=context)
        emprunts = emprunt_obj.browse(cr, uid, emprunt_ids, context=context)
        
        for emp in emprunts :
            for ech in emp.echeance_ids :
                mois=time.strftime('%m')
                annee=time.strftime('%y')
                if ech.date_remboursement_echeance[2:4]==annee :
                    if ech.date_remboursement_echeance[5:7]==mois :
                        cr.execute('update hr_echeance set statut_echeance =%s',(True))
        return True"""
    
    def process_sheet(self, cr, uid, ids, context=None):
        list=self.get_emprunt_mensuel(cr, uid, ids, context)
        for el in list :
            cr.execute("UPDATE hr_echeance set statut_echeance=True, date_remboursement_echeance=%s WHERE id=%s",
                       (date.today(),el.id))
            cr.commit()
        return super(hr_payslip,self).process_sheet(cr,uid,ids,context=context)
    
    def get_emprunt_mensuel(self,cr,uid,ids,context=None):
        for payslip in self.browse(cr, uid, ids, context=context):
            emprunts=payslip.employee_id.emprunt_ids
            list=[]
            for emp in emprunts:
                echeances = emp.echeance_ids
                for ech in echeances:
                    if payslip.date_from <=ech.date_prevu and ech.date_prevu<=payslip.date_to :
                        list+=[ech]
            return list
        
    def get_montant_emprunt(self,cr,uid,ids,context=None):
        employee_obj=self.pool.get('hr.employee')
        res={}
        for payslip in self.browse(cr, uid, ids, context=context):
            cr.execute("SELECT pl.amount\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.id = %s AND pl.slip_id=hp.id AND pl.code = %s",
                            (payslip.last_payslip.id, 'BASE'))
            test = cr.fetchone()
            raise osv.except_osv(_("Test"),test[0])

        
    _columns = {
                'emprunt':fields.boolean('Emprunt', help= "Appliquer le remboursement éventuel de l'emprunt"),
                }
    #_constraints=[(_compute_emprunt,"",[''])]
    _defaults = {
                 'emprunt':True,
                 }
hr_payslip()