# -*- encoding: utf-8 -*-

{
   'name' : 'Ap congés',
   'version' : '1.0',
   'author' : 'afica performance',
   'description' : 'autre client',
   'category': 'Enterprise Innovation',
   'website': '',
   'depends' : ['base','hr_holidays_extensions'], # liste des module a appeller lor d'un heritage (nom du model parent) ()
   'data' : ['ap_hr_holidays_ext_conges.xml'],# les fichiers de données à charger lors de l'installation du module
}