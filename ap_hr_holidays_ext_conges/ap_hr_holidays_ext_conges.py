# -*- coding: utf-8 -*-
##############################################################################
##############################################################################


import time
import math
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta

from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _


from openerp.tools.safe_eval import safe_eval as eval

class hr_employee(osv.osv):

    _inherit = 'hr.employee'

    def _get_max_leave(self, cr, uid, ids, field_names, arg, context=None):

        part = self.browse(cr, uid, ids, context=context)

        res= {}
        for number_of_days_temp in part:
            rav_obj=self.pool.get('hr.holidays')
            rav_ids= rav_obj.search(cr, uid, [('employee_id', '=', part.id)])
            rav = rav_obj.browse(cr, uid, rav_ids)
            total = 0.0
            for command in rav :
                if command.state == 'validate' and command.holiday_status_id.code == 'CONG' and command.type =='add':
                    total += command.number_of_days_temp
                    res[number_of_days_temp.id] = total
        return res

    def _get_taken_leave(self, cr, uid, ids, field_names, arg, context=None):

        part = self.browse(cr, uid, ids, context=context)

        res= {}
        for number_of_days_temp in part:
            rav_obj=self.pool.get('hr.holidays')
            rav_ids= rav_obj.search(cr, uid, [('employee_id', '=', part.id)])
            rav = rav_obj.browse(cr, uid, rav_ids)
            total = 0.0
            for command in rav :
                if command.state == 'validate' and command.holiday_status_id.code == 'CONG' and command.type =='remove':
                    total += command.number_of_days_temp
                    res[number_of_days_temp.id] = total
        return res

    _columns = {
                "max_leaves":fields.function(_get_max_leave,method=True,type="integer",string="Total jours congés"),
                "taken_leaves":fields.function(_get_taken_leave,method=True,type="integer",string="Total jours congés pris"),}


class hr_payslip(osv.osv):
    _name = 'hr.payslip'
    _inherit = 'hr.payslip'

    _constraints = []

    @api.cr_uid_ids_context
    def get_all_structures_conges(self, cr, uid, ids, struct_id, context=None):
        """
        @param contract_ids: list of contracts
        @return: the structures linked to the given contracts, ordered by hierachy (parent=False first, then first level children and so on) and without duplicata
        """
        for payslip in self.browse(cr, uid, ids, context=context):
            structure_ids = payslip.struct_id.id
        return structure_ids


    # def get_payslip(self, cr, uid, employee, date_from, date_to, context=None):
    #     """
    #     @param employee: browse record of employee
    #     @param date_from: date field
    #     @param date_to: date field
    #     @return: returns the ids of all the contracts for the given employee that need to be considered for the given dates
    #     """
    #     for payslip in self.browse(cr, uid, ids, context=context):
    #         payslip_id = payslip.id
    #     return payslip_id


    def compute_sheet(self, cr, uid, ids, context=None):
        slip_line_pool = self.pool.get('hr.payslip.line')
        sequence_obj = self.pool.get('ir.sequence')
        for payslip in self.browse(cr, uid, ids, context=context):
            number = payslip.number or sequence_obj.get(cr, uid, 'salary.slip')
            #delete old payslip lines
            old_slipline_ids = slip_line_pool.search(cr, uid, [('slip_id', '=', payslip.id)], context=context)
#            old_slipline_ids
            if old_slipline_ids:
                slip_line_pool.unlink(cr, uid, old_slipline_ids, context=context)
            if payslip.contract_id:
                #set the list of contract for which the rules have to be applied
                contract_ids = [payslip.contract_id.id]
            else:
                #if we don't give the contract, then the rules to apply should be for all current contracts of the employee
                contract_ids = self.get_contract(cr, uid, payslip.employee_id, payslip.date_from, payslip.date_to, context=context)
            for slip in payslip :
                if slip.struct_id == slip.contract_id.struct_id :
                    lines = [(0,0,line) for line in self.pool.get('hr.payslip').get_payslip_lines(cr, uid, contract_ids, payslip.id, context=context)]
                    self.write(cr, uid, [payslip.id], {'line_ids': lines, 'number': number,}, context=context)
                if slip.struct_id != slip.contract_id.struct_id :
                    lines = [(0,0,line) for line in self.pool.get('hr.payslip').get_payslip_lines_conges(cr, uid,contract_ids, payslip.id, context=context)]            
                    self.write(cr, uid, [payslip.id], {'line_ids': lines, 'number': number,}, context=context)
        return True



    def get_payslip_lines_conges(self, cr, uid, contract_ids, payslip_id, context):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
            return localdict
        
        class BrowsableObject(object):
            def __init__(self, pool, cr, uid, employee_id, dict):
                self.pool = pool
                self.cr = cr
                self.uid = uid
                self.employee_id = employee_id
                self.dict = dict

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(amount) as sum\
                            FROM hr_payslip as hp, hr_payslip_input as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()[0]
                return res or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours\
                            FROM hr_payslip as hp, hr_payslip_worked_days as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done'\
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                return self.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                self.cr.execute("SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s",
                            (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()
                return res and res[0] or 0.0
            
            
            def arrondi(self,montant):
                montant = Decimal(str(round(montant,0)))
                return float(montant)

            def arrondimilinf(self,montant):
                montant = Decimal(str(floor(montant / 1000)))
                return float(montant * 1000)
            
            def get_cumul_rubrique(self,code):
                if self.last_payslip == False :
                    return 0.0
                else :
                    annee=datetime.now().year
                    last_year=str(annee - 1)
                    if self.last_payslip.date_from[2:4]==last_year or self.last_payslip.date_to[2:4]==last_year :
                        return 0.0
                    else :
                        self.cr.execute("SELECT pl.total\
                                FROM hr_payslip as hp, hr_payslip_line as pl \
                                WHERE hp.id = %s AND pl.slip_id=hp.id AND pl.code = %s",
                                (self.last_payslip.id, code))
                        res=self.cr.fetchone()
                        return res[0]
    
            def last_amount(self,code): 
                if self.last_payslip == False :
                    return 0.0                  
                else :
                    self.cr.execute("SELECT pl.amount\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.id = %s AND pl.slip_id=hp.id AND pl.code = %s",
                            (self.last_payslip.id, code))
                    res=self.cr.fetchone()
                    return res[0]

               
                   
        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules = {}
        categories_dict = {}
        blacklist = []
        payslip_obj = self.pool.get('hr.payslip')
        inputs_obj = self.pool.get('hr.payslip.worked_days')
        obj_rule = self.pool.get('hr.salary.rule')
        payslip = payslip_obj.browse(cr, uid, payslip_id, context=context)
        worked_days = {}
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days[worked_days_line.code] = worked_days_line
        inputs = {}
        for input_line in payslip.input_line_ids:
            inputs[input_line.code] = input_line

        categories_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, categories_dict)
        input_obj = InputLine(self.pool, cr, uid, payslip.employee_id.id, inputs)
        worked_days_obj = WorkedDays(self.pool, cr, uid, payslip.employee_id.id, worked_days)
        payslip_obj = Payslips(self.pool, cr, uid, payslip.employee_id.id, payslip)
        rules_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, rules)

        localdict = {'categories': categories_obj, 'rules': rules_obj, 'payslip': payslip_obj, 'worked_days': worked_days_obj, 'inputs': input_obj}
        #get the ids of the structures on the contracts and their parent id as well
        structure_ids = payslip.struct_id.id
        #get the rules of the structure and thier children
        rule_ids = self.pool.get('hr.payroll.structure').get_all_rules(cr, uid, structure_ids, context=context)
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]

        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            employee = contract.employee_id
            localdict.update({'employee': employee, 'contract': contract})
            for rule in obj_rule.browse(cr, uid, sorted_rule_ids, context=context):
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                #check if the rule can be applied
                if obj_rule.satisfy_condition(cr, uid, rule.id, localdict, context=context) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = obj_rule.compute_rule(cr, uid, rule.id, localdict, context=context)
                    #check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'group_id': rule.group_id.id,
                        'group_sequence': rule.group_id.sequence,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in self.pool.get('hr.salary.rule')._recursive_search_of_rules(cr, uid, [rule], context=context)]

        result = [value for code, value in result_dict.items()]
        return result
    




