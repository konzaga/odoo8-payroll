# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd. (<http://devintellecs.com>).
#
##############################################################################


from openerp import models, fields


class hr_payslip(models.Model):
    _inherit = 'hr.payslip'

    
    
    identification_id = fields.Char(related='employee_id.name', store=True)
    payment_method = fields.Selection(related='employee_id.payment_method', store=True)
    