# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    
#
##############################################################################

{
    'name' : 'payslip extension',
	'version' : '2.0',
	'category': 'Generic Modules/Human Resources',
    'sequence':1,
    'summary': 'Employee indentification',
	'description' : """Prints Reports about employee's post""",
	'author' : 'Diaby Drissa',
	'images': [],
	'depends' : ['hr_payroll'],
	'data' : [
				'view/identification.xml',
		],	
	'demo' : [],
	'test' : [],
    'qweb' : [],
    'installable' : True,
	'auto_install' : False,	
	'application' : True,
    	
}




	
